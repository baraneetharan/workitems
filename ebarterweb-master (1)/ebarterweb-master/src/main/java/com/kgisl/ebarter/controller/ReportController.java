package com.kgisl.ebarter.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kgisl.ebarter.model.Brokerage;
import com.kgisl.ebarter.utilities.MysqlConnect;

/**
 * ReportController
 */
public class ReportController extends HttpServlet {
    private static final String filename = "application.properties";
    List<Brokerage> tradeList ;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Properties prop = new Properties();
        InputStream input = null;
        input = BrokerageController.class.getClassLoader().getResourceAsStream(filename);
        prop.load(input);

        double gst = Double.parseDouble(prop.getProperty("gst"));
        double stt = Double.parseDouble(prop.getProperty("stt"));
        double brkg = Double.parseDouble(prop.getProperty("brokerage"));
        String query = "SELECT * FROM trade";
        try {
            List<Object> list = MysqlConnect.getDbCon().resultSetToArrayList(query);
            List<Brokerage> tradeList = (List<Brokerage>) (List<?>) list;
            // tradeList.forEach(System.out::println);
            System.out.println(tradeList.size());
            HashSet<Brokerage> brokerageList = new HashSet<Brokerage>();
        brokerageList.addAll(tradeList);

        tradeList.forEach(System.out::println);

        // brokerageList.forEach(f -> f.setAmount(f.getQuantity() * f.getPrice()));

        // double brkg = 0.0125;
        // double gst = 0.18;
        // double stt = 0.00017;
        brokerageList.forEach(f -> {
            f.setAmount(f.getQuantity() * f.getPrice());
            f.setBrokerage(f.getAmount() * brkg);
            // System.out.println(f.getBrokerage() + "*" + gst + "->" + (f.getBrokerage() *
            // gst));
            f.setGst(f.getBrokerage() * gst);
            f.setSt(f.getAmount() * stt);
            if (f.getTradetype() == 'B') {
                f.setNetamount(f.getBrokerage() + f.getGst() + f.getSt() + f.getAmount());
            } else {
                f.setNetamount(f.getAmount() - (f.getBrokerage() + f.getGst() + f.getSt()));
            }
        });

        brokerageList.forEach(System.out::println);

        // Answers
        System.out.println("\nTop Buy scrip in a month by Netamount");
        Predicate<Brokerage> pre = x -> x.getTradetype() == 'B';
        List<Brokerage> ll = tradeList.stream().filter(pre).collect(Collectors.toList());
        ll.stream().max(Comparator.comparing(y -> y.getNetamount())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nTop Sell scrip in a month by Netamount");
        Predicate<Brokerage> pre1 = x -> x.getTradetype() == 'S';
        List<Brokerage> l1 = tradeList.stream().filter(pre1).collect(Collectors.toList());
        l1.stream().max(Comparator.comparing(y -> y.getNetamount())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nTop Turnover scrip ");
        tradeList.stream().max(Comparator.comparing(y -> y.getAmount())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nLow Turnover scrip ");
        tradeList.stream().min(Comparator.comparing(y -> y.getAmount())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nNet pay for each person for given date ");

        System.out.println("\nTotal Brokerage for a day ");
        double sum1 = tradeList.stream().mapToDouble(Brokerage::getBrokerage).sum();
        System.out.println(sum1);

        System.out.println("\nHighest Brokerage collection ");
        tradeList.stream().max(Comparator.comparing(y -> y.getBrokerage())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nAvg Buy price per scrip");
        OptionalDouble sum2 = ll.stream().mapToDouble(Brokerage::getPrice).average();
        System.out.println(sum2);

        // System.out.println(ta);
        System.out.println("\nAvg Sell price per scrip");
        OptionalDouble sum3 = l1.stream().mapToDouble(Brokerage::getPrice).average();
        System.out.println(sum3);

        System.out.println("\nTotal Brokerage for a person");

        System.out.println("\nHighest Brokerage paid on wich date by a person");
        tradeList.stream().max(Comparator.comparing(y -> y.getBrokerage())).map(Brokerage::getTradedate)
                .ifPresent(p -> System.out.println("" + p));

        System.out.println("\nCurrent holding for a person / day");

        System.out.println("\nWho is holding highest share ");
        tradeList.stream().max(Comparator.comparing(y -> y.getQuantity())).ifPresent(p -> System.out.println("" + p));

        System.out.println("\nWho is doing tranaction on every day");

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}