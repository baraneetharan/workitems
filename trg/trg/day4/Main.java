import java.util.Arrays;

/**
 * Main
 */
public class Main {
public static void main(String[] args) {
    System.out.println("Main Class is working");
    int a[]=new int[5];//declaration and instantiation 
    
a[0]=10;//initialization  
for(int i=0;i<a.length;i++)//length is the property of array  {}
System.out.println(a[i]); 

int a1[]={33,3,4,5};//declaration, instantiation and initialization  

for (int i : a1) {
    System.out.println(i);   
}

System.out.println(Arrays.toString(a1));

// Object Array

Emp emps[]=new Emp[5];
Emp e1=new Emp();
e1.id=1;
e1.name="Emp1";

emps[0]=e1;



System.out.println(Arrays.toString(emps));
System.out.println(Arrays.deepToString(emps));

Person[] people = { new Person(1,"Fred"), new Person(2,"Mike") };
System.out.println(Arrays.deepToString(people));


}
    
}