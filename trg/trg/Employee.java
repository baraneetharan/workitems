/**
 * Employee
 */
public class Employee {

    private int empid;
    private String name;

    /**
     * @return the empid
     */
    public int getEmpid() {
        return empid;
    }
    /**
     * @param empid the empid to set
     */
    public void setEmpid(int empid) {
        this.empid = empid;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        
        return empid + name;
    }
    
}