package com.kgisl.mvndemo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;

/**
 * CalcTest
 */
@FixMethodOrder()	
public class CalcTest {

@BeforeClass    
public static void beforeClass(){
    System.out.println("BeforeClass");
}
@Before
public void before(){
    System.out.println("Before");
}
@After
public void after(){
    System.out.println("After");
}    
@Test
public void addTest(){
    System.out.println("Add Test Method");
}
@Test
public void subTest(){
    System.out.println("Sub Test Method");
}

@Ignore
public void mulTest(){
    System.out.println("Multiply Test Method");
}
@AfterClass
public static void afterClass(){
    System.out.println("AfterClass");
}

}