package com.kgisl.unittest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * CalculatorTest
 */
@RunWith(Parameterized.class)
 public class CalculatorTest {
    private int fn;
    private int sn;
    private int er;
    private Calculator c;
    // = new Calculator();

    public CalculatorTest(int fn, int sn, int er) {
        this.fn = fn;
        this.sn = sn;
        this.er = er;
    }

    @Before
    public void initialize(){
        c=new Calculator();
    }
@Parameters
    public static Collection input(){
        return Arrays.asList(new Object [][]{{1,2,3},{11,22,33}});
    }
    @Test
    public void addTest() {
        int actual = c.add(fn, sn);
        int expected = 30;
        assertEquals(er, actual);
    }

    @Ignore
    public void subTest() {
        int actual = c.sub(fn, sn);
        int expected = -10;
        assertEquals(er, actual);
    }

    @Ignore
    public void mulTest() {
        int actual = c.mul(fn, sn);
        int expected = 200;
        assertEquals(er, actual);
    }

    @Ignore
    public void divTest() {
        int actual = c.div(100, 20);
        int expected = 5;
        assertEquals(er, actual);
    }

}