import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * CollegeTest
 */
public class CollegeTest {

    public static void main(String[] args) {
        College s1 = new College(1, "name1", "college1", "ECE", 1, 50, 52, 33);
        College s2 = new College(2, "name2", "college1", "IT", 1, 40, 32, 41);
        College s3 = new College(3, "name3", "college2", "ECE", 4, 45, 54, 76);
        College s4 = new College(4, "name4", "college3", "CSE", 1, 65, 80, 58);
        College s5 = new College(5, "name5", "college3", "CSE", 3, 32, 65, 49);
        College s6 = new College(6, "name6", "college3", "EEE", 2, 47, 51, 33);
        College s7 = new College(7, "name7", "college3", "IT", 4, 68, 72, 56);
        College s8 = new College(8, "name8", "college4", "CIVIL", 3, 72, 13, 72);
        College s9 = new College(9, "name9", "college4", "CSE", 2, 81, 28, 48);
        College s10 = new College(10, "name10", "college1", "IT", 2, 18, 69, 72);

        List<College> students = new ArrayList<>();
        students.add(s1);
        students.add(s2);
        students.add(s3);
        students.add(s4);
        students.add(s5);
        students.add(s6);
        students.add(s7);
        students.add(s8);
        students.add(s9);
        students.add(s10);

        System.out.println("\n****** All Students ******\n");

        students.forEach(System.out::println);

        List<String> totalMarks = students.stream()
                .map(x -> x.getId() + "," + (x.getMark1() + x.getMark2() + x.getMark3())).collect(Collectors.toList());
        System.out.println(totalMarks);

        System.out.println("\n****** Passed Students ******\n");
        // students.stream().filter(t -> t.getMark1() > 40 && t.getMark2() > 40 &&
        // t.getMark3() > 40)
        // .forEach(System.out::println);

        Predicate<College> findPassFail = t -> t.getMark1() > 40 && t.getMark2() > 40 && t.getMark3() > 40;
        students.stream().filter(findPassFail).map(x -> x.getId() + "," + (x.getMark1() + x.getMark2() + x.getMark3()))
                .forEach(System.out::println);

                

        // Predicate<College> findPassFail = new Predicate<College>() {

        // @Override
        // public boolean test(College t) {
        // return t.getMark1() > 40;
        // }
        // };

        
    }
}