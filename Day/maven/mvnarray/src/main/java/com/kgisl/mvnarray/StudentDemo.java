package com.kgisl.mvnarray;

import java.util.Arrays;
import java.util.Scanner;

/**
 * StudentDemo
 */
public class StudentDemo {
    static Student[] students = new Student[3];

    public static void main(String[] args) {
        loadStudents();
        getAllStudents();
        addStudent();
        getAllStudents();
        updateStudent();
        getAllStudents();
        // int[] numbers=new int[3];
        // numbers[0]=10;

        // Student[] students = new Student[3];
        // students[0]=new Student(1, "name");

        // System.out.println(Arrays.toString(numbers));
        // System.out.println(Arrays.toString(students));

    }

    public static void loadStudents() {
        students[0] = new Student(101, "ramu");
        students[1] = new Student(12, "Divya");
        // students[2] = new Student(11, "Janani");
    }

    public static void getAllStudents() {
        System.out.println(Arrays.toString(students));
    }

    public static void addStudent() {
        System.out.println("Enter rollno");
        Scanner in = new Scanner(System.in);
        int rollno = in.nextInt();
        System.out.println("Enter name");
        Scanner na = new Scanner(System.in);
        String name = na.nextLine();

        students[2] = new Student(rollno, name);
    }

    public static void updateStudent() {
        System.out.println("Enter name to be updated");
        Scanner na = new Scanner(System.in);
        String name = na.nextLine();
        for (Student var : students) {

            if (var.getName().equals(name)) {
                System.out.println(var.getName() + " " + name);
                var.setName("name");
                break;
            }
        }
    }
}