package com.kgisl.mvnarray;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * StudentTest
 */
public class StudentTest {

    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();// Creating arraylist

        System.out.println(list.size());

        list.add(new Student(1, "RamPriya"));// Adding object in arraylist
        list.add(new Student(3, "Vijay"));
        list.add(new Student(2, "Ravi"));
        list.add(new Student(4, "Ajay"));

        Student s1 = new Student(4, "sanjay");
        Student s2 = new Student(3, "Vijay");
        System.out.println(list.size());
        System.out.println(list);
        // studentList.set(studentList.indexOf(student),updateStudent);
        System.out.println(list.indexOf(s2));
        // list.set(list.indexOf(s2.),s1);
        list.set(3,s1);
        System.out.println(list);
    }
}