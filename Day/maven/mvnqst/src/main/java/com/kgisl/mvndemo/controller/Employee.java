package com.kgisl.mvndemo.controller;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Employee
 */
public class Employee {
    public static int[] evennumbers={2,4,8,10};    
    public static int[] oddnumbers={1,3,5,7};
    public static int[] numbers=new int[10];
    public static void main(String[] args) {
        System.out.println("Employee");     
        display();
        add();
        display();
    }

    public static void display(){
        System.out.println(Arrays.toString(numbers));
    }
    public static void add(){
        // numbers[0]=10;
        // numbers[1]=110;
        // numbers[2]=101;
        // numbers[3]=410;
        // numbers[4]=107;

        numbers=ArrayUtils.addAll(numbers, evennumbers);
    }

}