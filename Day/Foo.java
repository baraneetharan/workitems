class Foo{ 
      
    public static String name = ""; 
      
    // Instance method to be called within the same class or  
    // from a another class defined in the same package 
    // or in different package.  
    public static void geek(String name){ 
          
        name = name; 
    } 
} 