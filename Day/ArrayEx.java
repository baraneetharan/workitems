import java.util.Arrays;

/**
 * ArrayEx
 */
public class ArrayEx {

    public static void main(String[] args) {
        Employee[] copyFrom = new Employee[10];
        System.out.println(Arrays.toString(copyFrom));

        for (int i = 0; i < copyFrom.length; i++) {
            copyFrom[i] = new Employee();
        }
        System.out.println(Arrays.toString(copyFrom));

        Employee e1 = new Employee();
        e1.setId(1);
        e1.setName("name1");

        copyFrom[1] = e1;

        System.out.println(Arrays.toString(copyFrom));

        int searchid = 1;
        for (Employee var : copyFrom) {
            if (searchid == var.getId()) {
                System.out.println(var);
            }
        }

       System.out.println(Arrays.binarySearch(copyFrom, 1));
    }

}