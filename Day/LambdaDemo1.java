import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * LambdaDemo
 */
public class LambdaDemo1 {
    public static void main(String[] args) {

        // List<Integer> numberList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // Predicate<Integer> odd=new Predicate<Integer>() {
        // @Override
        // public boolean test(Integer t) {
        // return t%2!=0;
        // }
        // };

        // // Predicate<Integer> odd=x->x%2!=0;
        // // Predicate<Integer> even=x->x%2==0;

        // numberList.stream().filter(odd).forEach(System.out::println);
        // numberList.stream().filter(odd.negate()).forEach(System.out::println);

        // Scanner reader = new Scanner(System.in);

        // System.out.print("Enter a number: ");
        // int num = reader.nextInt();

        // String evenOdd = (num % 2 == 0) ? "even" : "odd";

        System.out.println((new Scanner(System.in).nextInt() % 2 == 0) ? "even" : "odd");

        // List<Staff> staff = Arrays.asList(new Staff("mkyong", 30), new Staff("jack", 27), new Staff("lawrence", 33));
        // //Java 8
        // List<String> collect = staff.stream().map(x -> x.getName()).collect(Collectors.toList());
        // System.out.println(collect); //[mkyong, jack, lawrence]
    }

}