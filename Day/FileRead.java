import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileRead {
    public static void main(String[] args) throws Exception {
        List<String> col1 = new ArrayList<>();
        col1.add("Jean");
        col1.add("author");
        col1.add("Java");
        List<String> col2 = new ArrayList<>();
        col2.add("David");
        col2.add("editor");
        col2.add("Python");
        List<String> col3 = new ArrayList<>();
        col3.add("Scott");
        col3.add("editor");
        col3.add("Node.js");

        List<List<String>> rows = new ArrayList<>();
        rows.add(col1);
        rows.add(col2);
        rows.add(col3);

        FileWriter csvWriter = new FileWriter("new.csv");
        csvWriter.append("Name");
        csvWriter.append(",");
        csvWriter.append("Role");
        csvWriter.append(",");
        csvWriter.append("Topic");
        csvWriter.append("\n");

        for (List<String> rowData : rows) {
            csvWriter.append(String.join(",", rowData));
            csvWriter.append("\n");
        }

        csvWriter.flush();
        csvWriter.close();
    }
}