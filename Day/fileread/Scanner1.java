import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Scanner1
 */
public class Scanner1 {

    public static void main(String[] args) {
        String file = "sample.txt";
        FileReader fr;
        try {
            fr = new FileReader(file);
            BufferedReader buffer = new BufferedReader(fr);
            String line;
            long length = 0;
            String[] data1 = null;
            try {
                while ((line = buffer.readLine()) != null) {
                    // if (line.isEmpty()) {
                    // break;
                    // }
                    data1 = line.split(",");
                    for (String var : data1) {
                        System.out.println(var);
                    }
                    length += line.length();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}