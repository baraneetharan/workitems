import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * BufferedReader1
 */
public class BufferedReader1 {

    public static void main(String[] args) {
        String file = "sample.txt";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String currentLine;
            try {
                currentLine = reader.readLine();
                System.out.println(currentLine);
                reader.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

    }
}