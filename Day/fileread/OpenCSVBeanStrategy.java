import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * OpenCSVBeanStrategy
 */
public class OpenCSVBeanStrategy {

    public static void main(String[] args) {
        String line;
        long length = 0;
        String[] data1 = null;

        String file = "sample.txt";
        FileReader fr;
        try {
            fr = new FileReader(file);
            BufferedReader buffer = new BufferedReader(fr);
            try {
                // System.out.println(buffer.lines().count()); 
                while ((line = buffer.readLine()) != null) {
                    // data1 = line.split(",");
                    // for (String var : data1) {
                    //     System.out.println(var);
                    // }
                    length += line.length();
                }
                System.out.println(length);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
}