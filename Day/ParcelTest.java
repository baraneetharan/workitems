// https://stackoverflow.com/questions/26684562/whats-the-difference-between-map-and-flatmap-methods-in-java-8

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 1
 */
public class ParcelTest {
    public static void main(String[] args) {
        Parcel amazon = new Parcel("amazon", "Laptop", "Phone");
        Parcel ebay = new Parcel("ebay", "Mouse", "Keyboard");
        List<Parcel> parcels = Arrays.asList(amazon, ebay);
    
        System.out.println("-------- Without flatMap() ---------------------------");
        List<List<String>> mapReturn = parcels.stream()
          .map(Parcel::getItems)
          .collect(Collectors.toList());
        System.out.println("\t collect() returns: " + mapReturn);
    
        System.out.println("\n-------- With flatMap() ------------------------------");
        List<String> flatMapReturn = parcels.stream()
          .map(Parcel::getItems)
          .flatMap(Collection::stream)
          .collect(Collectors.toList());
        System.out.println("\t collect() returns: " + flatMapReturn);
      }
    }