import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * OptionalEx
 */
public class OptionalEx {

    public static void main(String[] args) {
        String[] str = new String[10];
        // str[9]="Hello";
        Optional<String> isNull = Optional.ofNullable(str[9]);
        // Creating Optional object from a String
        Optional<String> GOT = Optional.of("Game of Thrones");
        // Optional.empty() creates an empty Optional object
        Optional<String> nothing = Optional.empty();
        /*
         * isPresent() method: Checks whether the given Optional Object is empty or not.
         */
        if (GOT.isPresent()) {
            System.out.println("Watching Game of Thrones");
        } else {
            System.out.println("I am getting Bored");
        }
        if (nothing.isPresent()) {
            System.out.println("Watching Game of Thrones");
        } else {
            System.out.println("I am getting Bored");
        }
        if (isNull.isPresent()) {
            System.out.println("Watching Game of Thrones");
        } else {
            System.out.println("I am getting Bored");
        }
        /*
         * ifPresent() method: It executes only if the given Optional object is
         * non-empty.
         */
        // This will print as the GOT is non-empty
        GOT.ifPresent(s -> System.out.println("Watching GOT is fun!"));
        // This will not print as the nothing is empty
        nothing.ifPresent(s -> System.out.println("I prefer getting bored"));
        Optional<String> GOT1 = Optional.of("Game of Thrones");
        System.out.println(GOT1.filter(s -> s.equals("GAME OF THRONES")));
        System.out.println(GOT1.filter(s -> s.equalsIgnoreCase("GAME OF THRONES")));

        List<Student> persons = Arrays.asList(new Student(1, "ani", 21));
        List<String> y = persons.stream().map(x -> x.age + x.name).collect(Collectors.toList());
        persons.forEach(System.out::println);
        y.forEach(System.out::println);

        //Creating Optional object from a String
        Optional<String> GOT2 = Optional.of("Game of Thrones");        
        //Optional.empty() creates an empty Optional object        
        Optional<String> nothing2 = Optional.empty();

        //orElse() method
        System.out.println(GOT2.orElse("Default Value")); 
        System.out.println(nothing2.orElse("Default Value")); 

        //orElseGet() method
        System.out.println(GOT2.orElseGet(() -> "Default Value")); 
        System.out.println(nothing2.orElseGet(() -> "Default Value")); 
    }
}