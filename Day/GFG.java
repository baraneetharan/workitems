import java.util.function.Function;

class GFG {
    public static void main(String[] args) {

        // create an instance of the class.
        Foo ob = new Foo();
        System.out.println(ob.name);
        // calling an instance method in the class 'Foo'.
        ob.geek("GeeksforGeeks");
        System.out.println(ob.name);

        String number = "10";
        int result = Integer.parseInt(number);
        System.out.println(result);
    }

}