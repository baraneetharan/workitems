import java.util.Arrays;
import java.util.List;

public class Parcel {
    private String name;
    private List<String> items;

    public Parcel(String name, String... items) {
        this.setName(name);
        this.setItems(Arrays.asList(items));
    }

    /**
     * @return the items
     */
    public List<String> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<String> items) {
        this.items = items;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

 
}