import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ServletLifeCycle
 */
public class ServletLifeCycle extends HttpServlet{
// System.out.println("ServletLifeCycle class called");
    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("init method called");
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        System.out.println("doget method called");
        res.setContentType("text/html");
        PrintWriter pw = res.getWriter();
        pw.println("Hello, Welcome to KGiSL...!");
        pw.close();
    }
    @Override
    public void destroy() {
        System.out.println("destroy method called");
    }
}