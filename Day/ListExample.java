import java.util.ArrayList;
import java.util.List;

public class ListExample {

    public static void main(String[] args) {
        List<Object> models = new ArrayList<>();
        Model m1 = new Model();
        Model m2 = new Model();
        Model m3 = new Model();

        m1.setId(1);
        m1.setName("name1");

        m1.setId(2);
        m2.setName("name2");

        m1.setId(3);
        m3.setName("name3");

        models.add(m1);
        models.add(m2);
        models.add(m3);
        models.forEach(System.out::println);

        // List<Model> models1 = new ArrayList<>();
        System.out.println("\n");
        List<Model> models1 = (List<Model>)(List<?>)models;
        models1.forEach(System.out::println);
    }
}