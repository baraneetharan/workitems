import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * LambdaDemo
 */
public class LambdaDemo {
    public static void main(String[] args) {
        
        List<Integer> numberList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // for (Integer i : numberList) {
        
        //         System.out.println(i);
        

        // }


        // Consumer<Integer> x = s -> System.out.println(s);
        Consumer<Integer> con=new Consumer<Integer>() {

            @Override
            public void accept(Integer t) {
                System.out.println(t);
            }
        };
        System.out.println("******");
        // numberList.forEach(x);
        numberList.forEach(con);


        Consumer<Integer> con1= t -> System.out.println(t);
        numberList.forEach(con1);

        Predicate<Integer> odd=x->x%2!=0;
        Predicate<Integer> even=x->x%2==0;
        numberList.stream().filter(odd).forEach(System.out::println);
        numberList.stream().filter(even).forEach(System.out::println);
    }


}