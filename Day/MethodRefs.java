import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * MethodRefs
 */
public class MethodRefs {

    public static void main(String[] args) {
        List<Integer> integerNumbers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        // className::new
        List<Double> doubleNumbers = MethodRefs.applyFunction(integerNumbers, Double::new);
        System.out.println(doubleNumbers);
    }

    public static List<Double> applyFunction(List<Integer> list, Function<Integer, Double> function) {
        List<Double> returnList = new ArrayList<>();
        list.forEach(x -> returnList.add(function.apply(x)));
        return returnList;
    }
}