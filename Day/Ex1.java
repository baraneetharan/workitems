import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Ex1
 */
public class Ex1 {

    public static void main(String[] args) {
        String commaSeparated = "item1 , item2 , item3";
        List<String> list = new ArrayList<String>(Arrays.asList(commaSeparated.split(" , ")));
        System.out.println(list);

        ArrayList<String> names = new ArrayList<String>();
        names.add("seetha");
        names.add("sudhin");
        names.add("Swetha");
        names.add("Neethu");
        names.add("ananya");
        names.add("Athira");
        names.add("bala");
        names.add("Tony");
        names.add("Karthika");
        names.add("Nithin");
        names.add("Vinod");
        names.add("jeena");

        List<String> sorted = Arrays
                .asList(names.stream().sorted((s1, s2) -> s1.compareToIgnoreCase(s2)).toArray(String[]::new));
        System.out.println(sorted);

        System.out.println(names.stream().sorted().collect(Collectors.toList()));

        Collections.sort(names);
        System.out.println(names);

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });

        System.out.println(names);

        names.sort(String::compareToIgnoreCase);
        System.out.println(names);
    }
}