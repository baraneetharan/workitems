import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * EmployeeTest
 */
public class EmployeeTest {

    public static void main(String[] args) {
        Employee employee = new Employee();
        // employee.id=1;
        // employee.name="Employee1";
        employee.setId(0);
        employee.setName("Employee0");

        Employee employee1 = new Employee();
        employee1.setId(1);
        employee1.setName("Employee1");

        Employee employee2 = new Employee();
        employee2.setId(502);
        employee2.setName("Employee2");

        Employee employee3 = new Employee();
        employee3.setId(202);
        employee3.setName("Employee3");

        Employee[] employees = new Employee[3];
        employees[0] = employee;
        employees[1] = employee1;
        employees[2] = employee2;

        ArrayList<Employee> al=new ArrayList<>();
        al.add(employee);
        al.add(employee1);
        al.add(employee2);
        al.add(employee3);

        for (Employee var : employees) {
            System.out.println(var);
        }

        System.out.println(Arrays.toString(employees));
        // System.out.println(employee);
        // System.out.println(employee1);

        // System.out.println(employee.hashCode());
        // System.out.println(employee1.hashCode());

        // System.out.println(employee.getClass());
        // System.out.println(employee1.getClass());

        Collections.sort(al);

        for (Employee var : al) {
            System.out.println(var.getId()+" "+var.getName());
        }

        System.out.println("EmployeeNameComparator");
        Collections.sort(al,new EmployeeNameComparator()); 
        for (Employee var : al) {
            System.out.println(var.getId()+" "+var.getName());
        }
    }
}