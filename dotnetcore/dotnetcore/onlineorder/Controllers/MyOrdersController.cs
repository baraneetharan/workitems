using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using onlineorder.Models;

namespace onlineorder.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class MyOrdersController : ControllerBase {
        private readonly MyOrderDBContext _context;

        public MyOrdersController (MyOrderDBContext context) {
            _context = context;
        }

        // GET: api/MyOrders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MyOrder>>> GetMyOrders () {
            return await _context.MyOrders.ToListAsync ();
        }

        // GET: api/MyOrders/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<MyOrder>> GetMyOrder (long id) {
            var myOrder = await _context.MyOrders.FindAsync (id);

            if (myOrder == null) {
                return NotFound ();
            }

            return myOrder;
        }

        // PUT: api/MyOrders/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutMyOrder (long id, MyOrder myOrder) {
            if (id != myOrder.Id) {
                return BadRequest ();
            }

            _context.Entry (myOrder).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!MyOrderExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/MyOrders
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MyOrder>> PostMyOrder (MyOrder myOrder) {
            _context.MyOrders.Add (myOrder);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetMyOrder", new { id = myOrder.Id }, myOrder);
        }

        [HttpPost]
        [Route ("readfromcsv")]

        public async Task<List<MyOrder>> readcsvAsync () {
            List<MyOrder> myOrderValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => MyOrder.FromCsv (v)).ToList ();
            foreach (MyOrder item in myOrderValues) {
                _context.MyOrders.Add (item);
                await _context.SaveChangesAsync ();
            }
            return myOrderValues;

        }
        // DELETE: api/MyOrders/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<MyOrder>> DeleteMyOrder (long id) {
            var myOrder = await _context.MyOrders.FindAsync (id);
            if (myOrder == null) {
                return NotFound ();
            }

            _context.MyOrders.Remove (myOrder);
            await _context.SaveChangesAsync ();

            return myOrder;
        }

        private bool MyOrderExists (long id) {
            return _context.MyOrders.Any (e => e.Id == id);
        }
    }
}