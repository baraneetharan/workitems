using System;

namespace onlineorder.Models {

    public class MyOrder {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public float Amount { get; set; }
public static MyOrder FromCsv (string csvLine) {
            string[] values = csvLine.Split (',');
            MyOrder myCartValues = new MyOrder ();
            myCartValues.Id = 0;
            myCartValues.Name = Convert.ToString (values[1]);
            myCartValues.Quantity = int.Parse(values[2]);//Convert.ToInt32 (values[2]);
            myCartValues.Price = float. Parse(values[3]);
            myCartValues.Amount = myCartValues.Quantity* myCartValues.Price;
            return myCartValues;
        }
    }
}