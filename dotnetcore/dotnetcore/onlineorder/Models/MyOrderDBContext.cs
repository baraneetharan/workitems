using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace onlineorder.Models{
public class MyOrderDBContext:DbContext
{
    public MyOrderDBContext(DbContextOptions<MyOrderDBContext> options):base(options){}

    
            public DbSet<MyOrder> MyOrders { get; set; }

}}