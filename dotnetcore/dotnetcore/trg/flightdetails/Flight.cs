using System;

class Flight {
    public string Flightid { get; set; }
    public string Airway { get; set; }
    public String Departure { get; set; }
    public String Arrival { get; set; }
    public Double Fare { get; set; }

    public Flight (string Flightid, string Airway, string Departure, string Arrival, Double Fare) {
        this.Flightid = Flightid;
        this.Airway = Airway;
        this.Departure = Departure;
        this.Arrival = Arrival;
        this.Fare = Fare;
    }

}