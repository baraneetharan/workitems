using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

class CarComparerByName : IComparer<Car>
{
    public int Compare([AllowNull] Car x, [AllowNull] Car y)
    {
        return x.Name.CompareTo(y.Name); 
    }
}