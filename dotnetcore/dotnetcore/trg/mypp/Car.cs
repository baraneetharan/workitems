using System;

class Car  {
    public string Name { get; set; }
    public int MaxSpeed { get; set; }

    public Car (string Name, int MaxSpeed) {
        this.Name = Name;
        this.MaxSpeed = MaxSpeed;
    }

    
}