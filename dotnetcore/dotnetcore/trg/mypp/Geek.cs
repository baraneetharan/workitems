using System;

public class Geek
{
    public string name; 
    public int id;

 public Geek(string name, int id) 
    { 
          
        this.name = name; 
        this.id = id; 
    }    

// override object.Equals
public override bool Equals(object obj)
{
        // if both the object references are  
    // referring to the same object. 
    if(this == obj) 
            return true; 
          
        // it checks if the argument is of the  
        // type Geek by comparing the classes  
        // of the passed argument and this object. 
        // if(!(obj instanceof Geek)) return false; ---> avoid. 
        if(obj == null || obj.GetType()!= this.GetType()) 
            return false; 
          
        // type casting of the argument.  
        Geek geek = (Geek) obj; 
          
        // comparing the state of argument with  
        // the state of 'this' Object. 
        return (geek.name==this.name  && geek.id == this.id); 

}

// override object.GetHashCode
public override int GetHashCode()
{
    return this.id;
}
}
