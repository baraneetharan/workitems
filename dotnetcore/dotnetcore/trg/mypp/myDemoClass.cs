

using System;

class myDemoClass {  
        public myDemoClass() {  
            Console.WriteLine("1");  
        }  
        public myDemoClass(int x): this() {  
            Console.WriteLine("2");  
        }  
        public myDemoClass(int x, int y): this(10) {  
            Console.WriteLine("3");  
        }  
    }  