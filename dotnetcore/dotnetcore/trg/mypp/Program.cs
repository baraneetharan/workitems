﻿using System;

namespace mypp {
    class Program {
        static void Main (string[] args) {
            User u = new User("Hyderabad", 31);

            // Declare object without new keyword

            User u1;

            Console.WriteLine("Name: {0}, Location: {1}, Age: {2}", User.name, u.location, u.age);

            // Initialize Fields

            u1.location = "Guntur";

            u1.age = 32;

            Console.WriteLine("Name: {0}, Location: {1}, Age: {2}", User.name, u1.location, u1.age);
        }
    }
}