using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

class CarComparerByMaxSpped : IComparer<Car>
{
    public int Compare([AllowNull] Car x, [AllowNull] Car y)
    {
        return x.MaxSpeed.CompareTo(y.MaxSpeed); 
    }
}