using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using webapi1.Models;
using webapp1.Models;

namespace webapp1.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase {
        private readonly TodoContext _context;
        private readonly MySettings _appSettings;

        // public TodoItemsController (TodoContext context, IOptions<MySettings> mysettings) {
        //     _context = context;
        //     _appSettings = mysettings;
        // }
        public TodoItemsController (TodoContext context, MySettings mysettings) {
            _context = context;
            _appSettings = mysettings;
        }
        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems () {
            var varGst = _appSettings.Value1;
            var varStt = _appSettings.Value2;
            // var varSt = _appSettings.Value.st;
            Console.WriteLine (varGst);
            return await _context.TodoItems.ToListAsync ();
        }

        // GET: api/TodoItems/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem (long id) {
            var todoItem = await _context.TodoItems.FindAsync (id);

            if (todoItem == null) {
                return NotFound ();
            }

            return todoItem;
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutTodoItem (long id, TodoItem todoItem) {
            if (id != todoItem.Id) {
                return BadRequest ();
            }

            _context.Entry (todoItem).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!TodoItemExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem (TodoItem todoItem) {
            _context.TodoItems.Add (todoItem);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetTodoItem", new { id = todoItem.Id }, todoItem);
        }

        [HttpPost]
        [Route ("readfromcsv")]
        public async Task<List<TodoItem>> PostbrokerageCollection () {
            //  List<TodoItem> srcvalues = System.IO.File.ReadAllLines(@"C:\Users\gokulakrishnan.t\Desktop\sampleinput.csv").Skip(1).Select(v => TodoItem.FromCsv(v)).ToList();
            //  var Path = AppDomain.CurrentDomain.BaseDirectory + "\YourFolder";
            List<TodoItem> todoItemValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => TodoItem.FromCsv (v)).ToList ();
            foreach (TodoItem x in todoItemValues) {
                _context.TodoItems.Add (x);
                await _context.SaveChangesAsync ();
            }

            return todoItemValues;

        }

        // DELETE: api/TodoItems/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem (long id) {
            var todoItem = await _context.TodoItems.FindAsync (id);
            if (todoItem == null) {
                return NotFound ();
            }

            _context.TodoItems.Remove (todoItem);
            await _context.SaveChangesAsync ();

            return todoItem;
        }

        private bool TodoItemExists (long id) {
            return _context.TodoItems.Any (e => e.Id == id);
        }
    }
}