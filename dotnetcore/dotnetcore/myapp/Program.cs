﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace myapp {
    class Program {
        static SqlConnection con;
        static void Main (string[] args) {
            getConnection ();
            getAll ();
            create ();
            getAll ();
        }

        public static void getConnection () {
            con = new SqlConnection ("server=10.100.8.148;database=SchoolDB;user id=testing;password=testing");
        }

        public static void create () {
            //Fill Dataset
            // string ConString = @"Data Source=.\SQLEXPRESS;Initial Catalog=ComputerShop;Integrated Security=True";
            string Query = "SELECT * FROM student";
            SqlDataAdapter adapter = new SqlDataAdapter (Query, con);
            DataSet set = new DataSet ();
            adapter.Fill (set, "students");

            //Adding New Row to DataSet and Update           
            DataRow row = set.Tables["students"].NewRow ();
            row["StudentId"] = 456;
            row["Name"] = "Student2";
            set.Tables["students"].Rows.Add (row);

            //Updating Database Table
            SqlCommandBuilder builder = new SqlCommandBuilder (adapter);
            adapter.Update (set.Tables["students"]);
        }
        public static void getAll () {
            // SqlCommand cmd = new SqlCommand("select * from student", con);
            string Query = "select * from student";

            con.Open ();
            SqlDataAdapter adapter = new SqlDataAdapter (Query, con);
            DataSet ds = new DataSet ();

            adapter.Fill (ds, "Items");

            Console.WriteLine ("Tables in '{0}' DataSet.\n", ds.DataSetName);
            foreach (DataTable dt in ds.Tables) {
                Console.WriteLine ("{0} Table.\n", dt.TableName);
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
                    Console.Write (dt.Columns[curCol].ColumnName.Trim () + "\t");
                }
                for (int curRow = 0; curRow < dt.Rows.Count; curRow++) {
                    for (int curCol = 0; curCol < dt.Columns.Count; curCol++) {
                        Console.Write (dt.Rows[curRow][curCol].ToString ().Trim () + "\t");
                    }
                    Console.WriteLine ();
                }
            }

            con.Close ();
        }

        public static void getOne(){
            
        }
    }

}