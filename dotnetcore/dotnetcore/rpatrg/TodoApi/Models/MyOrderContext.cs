using Microsoft.EntityFrameworkCore;
namespace TodoApi.Models {
    public class MyOrderContext : DbContext {
        public MyOrderContext (DbContextOptions<MyOrderContext> options) : base (options) { }
        public DbSet<MyOrder> MyOrders { get; set; }

    }
}