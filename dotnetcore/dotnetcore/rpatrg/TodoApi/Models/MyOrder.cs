using System;

namespace TodoApi.Models {
    public class MyOrder {
        public int id { get; set; }
        public string name { get; set; }
        public double quantity { get; set; }
        public double price { get; set; }
        public double amount { get; set; }
        public double TotalCost {
            get { return quantity * price; }
        }

        public static MyOrder FromCsv (string csvLine) {
            string[] values = csvLine.Split (',');
            MyOrder myOrderValues = new MyOrder ();
            myOrderValues.id = 0;
            myOrderValues.name = Convert.ToString (values[1]);
            myOrderValues.quantity = int.Parse (values[2]);
            myOrderValues.price = float.Parse (values[3]);

            return myOrderValues;
        }
    }
}