using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class MyOrderController : ControllerBase {
        private readonly MyOrderContext _context;

        public MyOrderController (MyOrderContext context) {
            _context = context;
        }

        // GET: api/MyOrder
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MyOrder>>> GetMyOrders () {
            return await _context.MyOrders.ToListAsync ();
        }

        // GET: api/MyOrder/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<MyOrder>> GetMyOrder (int id) {
            var myOrder = await _context.MyOrders.FindAsync (id);

            if (myOrder == null) {
                return NotFound ();
            }

            return myOrder;
        }

        // PUT: api/MyOrder/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutMyOrder (int id, MyOrder myOrder) {
            if (id != myOrder.id) {
                return BadRequest ();
            }

            _context.Entry (myOrder).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!MyOrderExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/MyOrder
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MyOrder>> PostMyOrder (MyOrder myOrder) {
            _context.MyOrders.Add (myOrder);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetMyOrder", new { id = myOrder.id }, myOrder);
        }

        [HttpPost]
        [Route ("readfromcsv")]
        public async Task<List<MyOrder>>  readmycartsAsync () {
            List<MyOrder> myOrderValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => MyOrder.FromCsv (v)).ToList ();;
            _context.MyOrders.AddRange (myOrderValues);
            await _context.SaveChangesAsync ();
            return myOrderValues;
        }

        // DELETE: api/MyOrder/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<MyOrder>> DeleteMyOrder (int id) {
            var myOrder = await _context.MyOrders.FindAsync (id);
            if (myOrder == null) {
                return NotFound ();
            }

            _context.MyOrders.Remove (myOrder);
            await _context.SaveChangesAsync ();

            return myOrder;
        }

        private bool MyOrderExists (int id) {
            return _context.MyOrders.Any (e => e.id == id);
        }
    }
}