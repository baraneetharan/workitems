using lmsdemo.Models;
using Microsoft.EntityFrameworkCore;

namespace lmsdemo.Models
{
    public class PlaylistContext : DbContext
    {
        public PlaylistContext(DbContextOptions<PlaylistContext> options)
            : base(options)
        {
        }

        public DbSet<Playlist> PlaylistItems { get; set; }
    }
}