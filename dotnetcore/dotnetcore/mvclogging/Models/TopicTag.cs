using System.Collections.Generic;
using System.ComponentModel;

namespace mvclogging.Models
{public class TopicTag
{
    public int Id { get; set; }

    [DisplayName("Tag")]
    public string TagValue { get; set; }

    public List<LearningResourceTopicTag> LearningResourceTopicTags { get; set; }
} }