using System.Collections.Generic;

namespace mvclogging.Models
{public class ResourceList
{
    public int Id { get; set; }

    public string Name { get; set; }

    public List<LearningResource> LearningResources { get; set; }
} }