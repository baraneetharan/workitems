namespace mvclogging.Models
{
    public class LearningResourceTopicTag
    {
        public int LearningResourceId { get; set; }
        public LearningResource LearningResource { get; set; }

        public int TopicTagId { get; set; }
        public TopicTag TopicTag { get; set; }

    }
}