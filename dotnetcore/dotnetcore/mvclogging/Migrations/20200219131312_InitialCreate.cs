﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace mvclogging.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ResourceLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourceLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TopicTags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicTags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LearningResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    ResourceListId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LearningResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LearningResources_ResourceLists_ResourceListId",
                        column: x => x.ResourceListId,
                        principalTable: "ResourceLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContentFeeds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FeedUrl = table.Column<string>(nullable: true),
                    LearningResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContentFeeds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContentFeeds_LearningResources_LearningResourceId",
                        column: x => x.LearningResourceId,
                        principalTable: "LearningResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LearningResourceTopicTag",
                columns: table => new
                {
                    LearningResourceId = table.Column<int>(nullable: false),
                    TopicTagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LearningResourceTopicTag", x => new { x.LearningResourceId, x.TopicTagId });
                    table.ForeignKey(
                        name: "FK_LearningResourceTopicTag_LearningResources_LearningResourceId",
                        column: x => x.LearningResourceId,
                        principalTable: "LearningResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LearningResourceTopicTag_TopicTags_TopicTagId",
                        column: x => x.TopicTagId,
                        principalTable: "TopicTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContentFeeds_LearningResourceId",
                table: "ContentFeeds",
                column: "LearningResourceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LearningResources_ResourceListId",
                table: "LearningResources",
                column: "ResourceListId");

            migrationBuilder.CreateIndex(
                name: "IX_LearningResourceTopicTag_TopicTagId",
                table: "LearningResourceTopicTag",
                column: "TopicTagId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContentFeeds");

            migrationBuilder.DropTable(
                name: "LearningResourceTopicTag");

            migrationBuilder.DropTable(
                name: "LearningResources");

            migrationBuilder.DropTable(
                name: "TopicTags");

            migrationBuilder.DropTable(
                name: "ResourceLists");
        }
    }
}
