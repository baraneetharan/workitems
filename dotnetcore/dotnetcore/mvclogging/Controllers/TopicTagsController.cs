using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mvclogging.Models;

namespace mvclogging.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TopicTagsController : ControllerBase
    {
        private readonly LibDbContext _context;

        public TopicTagsController(LibDbContext context)
        {
            _context = context;
        }

        // GET: api/TopicTags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TopicTag>>> GetTopicTags()
        {
            return await _context.TopicTags.ToListAsync();
        }

        // GET: api/TopicTags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TopicTag>> GetTopicTag(int id)
        {
            var topicTag = await _context.TopicTags.FindAsync(id);

            if (topicTag == null)
            {
                return NotFound();
            }

            return topicTag;
        }

        // PUT: api/TopicTags/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTopicTag(int id, TopicTag topicTag)
        {
            if (id != topicTag.Id)
            {
                return BadRequest();
            }

            _context.Entry(topicTag).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicTagExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TopicTags
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TopicTag>> PostTopicTag(TopicTag topicTag)
        {
            _context.TopicTags.Add(topicTag);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTopicTag", new { id = topicTag.Id }, topicTag);
        }

        // DELETE: api/TopicTags/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TopicTag>> DeleteTopicTag(int id)
        {
            var topicTag = await _context.TopicTags.FindAsync(id);
            if (topicTag == null)
            {
                return NotFound();
            }

            _context.TopicTags.Remove(topicTag);
            await _context.SaveChangesAsync();

            return topicTag;
        }

        private bool TopicTagExists(int id)
        {
            return _context.TopicTags.Any(e => e.Id == id);
        }
    }
}
