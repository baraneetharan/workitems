﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using mvclogging.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace mvclogging.Controllers {
    public class HomeController : Controller {
        private readonly ILogger<HomeController> _logger;

        public HomeController (ILogger<HomeController> logger) {
            _logger = logger;
        }

        public IActionResult Index () {
            _logger.LogInformation ("Log message in the Index() method");
            _logger.Log (LogLevel.Error, "some text");
            _logger.Log (LogLevel.Debug, "some text");
            _logger.Log (LogLevel.Warning, "some text");
            _logger.Log (LogLevel.Trace, "some text");
            _logger.Log (LogLevel.Critical, "some text");

            return View ();
        }

        public IActionResult Privacy () {
            return View ();
        }

        [ResponseCache (Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error () {
            return View (new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}