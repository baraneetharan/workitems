using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mvclogging.Models;

namespace mvclogging.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResourceListsController : ControllerBase
    {
        private readonly LibDbContext _context;

        public ResourceListsController(LibDbContext context)
        {
            _context = context;
        }

        // GET: api/ResourceLists
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ResourceList>>> GetResourceLists()
        {
            return await _context.ResourceLists.ToListAsync();
        }

        // GET: api/ResourceLists/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ResourceList>> GetResourceList(int id)
        {
            var resourceList = await _context.ResourceLists.FindAsync(id);

            if (resourceList == null)
            {
                return NotFound();
            }

            return resourceList;
        }

        // PUT: api/ResourceLists/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutResourceList(int id, ResourceList resourceList)
        {
            if (id != resourceList.Id)
            {
                return BadRequest();
            }

            _context.Entry(resourceList).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResourceListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ResourceLists
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ResourceList>> PostResourceList(ResourceList resourceList)
        {
            _context.ResourceLists.Add(resourceList);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetResourceList", new { id = resourceList.Id }, resourceList);
        }

        // DELETE: api/ResourceLists/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ResourceList>> DeleteResourceList(int id)
        {
            var resourceList = await _context.ResourceLists.FindAsync(id);
            if (resourceList == null)
            {
                return NotFound();
            }

            _context.ResourceLists.Remove(resourceList);
            await _context.SaveChangesAsync();

            return resourceList;
        }

        private bool ResourceListExists(int id)
        {
            return _context.ResourceLists.Any(e => e.Id == id);
        }
    }
}
