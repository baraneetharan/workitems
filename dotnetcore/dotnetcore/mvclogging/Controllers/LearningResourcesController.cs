using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mvclogging.Models;

namespace mvclogging.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LearningResourcesController : ControllerBase
    {
        private readonly LibDbContext _context;

        public LearningResourcesController(LibDbContext context)
        {
            _context = context;
        }

        // GET: api/LearningResources
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LearningResource>>> GetLearningResources()
        {
            return await _context.LearningResources.ToListAsync();
        }

        // GET: api/LearningResources/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LearningResource>> GetLearningResource(int id)
        {
            var learningResource = await _context.LearningResources.FindAsync(id);

            if (learningResource == null)
            {
                return NotFound();
            }

            return learningResource;
        }

        // PUT: api/LearningResources/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLearningResource(int id, LearningResource learningResource)
        {
            if (id != learningResource.Id)
            {
                return BadRequest();
            }

            _context.Entry(learningResource).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LearningResourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LearningResources
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<LearningResource>> PostLearningResource(LearningResource learningResource)
        {
            _context.LearningResources.Add(learningResource);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLearningResource", new { id = learningResource.Id }, learningResource);
        }

        // DELETE: api/LearningResources/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LearningResource>> DeleteLearningResource(int id)
        {
            var learningResource = await _context.LearningResources.FindAsync(id);
            if (learningResource == null)
            {
                return NotFound();
            }

            _context.LearningResources.Remove(learningResource);
            await _context.SaveChangesAsync();

            return learningResource;
        }

        private bool LearningResourceExists(int id)
        {
            return _context.LearningResources.Any(e => e.Id == id);
        }
    }
}
