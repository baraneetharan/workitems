using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using mvc2.Models;

namespace mvc2.Controllers{


public class razortblsController : Controller
{
    public readonly razortblDBContext _context;
    public razortblsController(razortblDBContext context)
    {
        _context= context;
    }
        public async Task<IActionResult> index()
        {
            return View(await _context.razortbls.ToListAsync());
        }
        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

                // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,Name")] razortbl razortbls)
        {
            if (ModelState.IsValid)
            {
                _context.Add(razortbls);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(index));
            }
            return View(razortbls);
        }

// GET: Products/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var razor = await _context.razortbls.FindAsync(id);
            if (razor == null)
            {
                return NotFound();
            }
            return View(razor);
        }

                // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ProductId,Name")] razortbl razortbls)
        {
            if (id != razortbls.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(razortbls);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!razortblsExists(razortbls.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(index));
            }
            return View(razortbls);


        }
         // GET: Products/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var razor = await _context.razortbls
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (razor == null)
            {
                return NotFound();
            }

            return View(razor);
        }
// POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var products = await _context.razortbls.FindAsync(id);
            _context.razortbls.Remove(products);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(index));
        }

          // GET: Products/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var products = await _context.razortbls
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (products == null)
            {
                return NotFound();
            }

            return View(products);
        }
private bool razortblsExists(long id)
        {
            return _context.razortbls.Any(e => e.ProductId == id);
        }

    }
}