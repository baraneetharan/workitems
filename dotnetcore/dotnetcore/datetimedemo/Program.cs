﻿using System;

namespace datetimedemo {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine ("Hello World!");
            // The object describes the current time.
            DateTime now = DateTime.Now; 
            Console.WriteLine ("Now is " + now);
        }
    }
}