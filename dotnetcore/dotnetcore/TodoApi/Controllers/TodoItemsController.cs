using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase {
        private readonly TodoContext _context;

        public TodoItemsController (TodoContext context) {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems () {
            return await _context.TodoItems.ToListAsync ();
        }

        // GET: api/TodoItems/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem (long id) {
            var todoItem = await _context.TodoItems.FindAsync (id);

            if (todoItem == null) {
                return NotFound ();
            }

            return todoItem;
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutTodoItem (long id, TodoItem todoItem) {
            if (id != todoItem.Id) {
                return BadRequest ();
            }

            _context.Entry (todoItem).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!TodoItemExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem (TodoItem todoItem) {
            _context.TodoItems.Add (todoItem);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetTodoItem", new { id = todoItem.Id }, todoItem);
        }

        [HttpPost]
        [Route ("readfromcsv")]
        public async Task<List<TodoItem>> readmycartsAsync () {
            List<TodoItem> todoItemValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => TodoItem.FromCsv (v)).ToList ();;
            // foreach (TodoItem x in todoItemValues) {
            //     _context.TodoItems.Add (x);
            //     await _context.SaveChangesAsync ();
            // }
            // return todoItemValues;
            _context.TodoItems.AddRange (todoItemValues);
            await _context.SaveChangesAsync ();

            // https://github.com/borisdj/EFCore.BulkExtensions
            // await _context.BulkInsertAsync(todoItemValues); //<----- Only change

            return todoItemValues;
        }

        // DELETE: api/TodoItems/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem (long id) {
            var todoItem = await _context.TodoItems.FindAsync (id);
            if (todoItem == null) {
                return NotFound ();
            }

            _context.TodoItems.Remove (todoItem);
            await _context.SaveChangesAsync ();

            return todoItem;
        }

        private bool TodoItemExists (long id) {
            return _context.TodoItems.Any (e => e.Id == id);
        }
    }
}