using System;

namespace TodoApi.Models {
    public class TodoItem {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }

        public static TodoItem FromCsv (string csvLine) {
            string[] values = csvLine.Split (',');
            TodoItem todoItemValues = new TodoItem ();
            todoItemValues.Id = 0;
            todoItemValues.Name = Convert.ToString (values[1]);
            todoItemValues.IsComplete = Convert.ToBoolean (values[2]);
            return todoItemValues;
        }
    }
}