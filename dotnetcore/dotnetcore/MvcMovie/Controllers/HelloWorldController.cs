using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc;

namespace MvcMovie.Controllers {
    public class HelloWorldController : Controller {
        // 
        // GET: /HelloWorld/

        public IActionResult Index () {
            // return "This is my default action...";
                return View();

        }

        // 
        // GET: /HelloWorld/Welcome/ 
        // [ActionName ("find")]
            // [NonAction]

        public string Welcome (string name, int ID = 1) {
                // return HtmlEncoder.Default.Encode($"Hello {name}, NumTimes is: {numTimes}");
                return HtmlEncoder.Default.Encode($"Hello {name}, ID: {ID}");

        }
    }
}