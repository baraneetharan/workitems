using System;
using System.Threading.Tasks;
using mvc1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace mvc1.Controllers {
    public class Product23sController : Controller {
        private readonly InventoryContext _context;

        public Product23sController (InventoryContext context) {
            _context = context;
        }

        // GET: Products
        public async Task<IActionResult> Index () {            
            return View (await _context.Products23s.ToListAsync ());
        }
    }
}