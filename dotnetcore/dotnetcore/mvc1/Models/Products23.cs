using System;
using System.ComponentModel.DataAnnotations;

namespace mvc1.Models {
    public partial class Products23 {
        [Key]
        public long ProductId { get; set; }
        public string Name { get; set; }
    }
}