using Microsoft.EntityFrameworkCore;

namespace mvc1.Models {
    public partial class InventoryContext : DbContext {
        public InventoryContext () { }

        public InventoryContext (DbContextOptions<InventoryContext> options) : base (options) { }

        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Products23> Products23s { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<Products> (entity => {
                entity.HasKey (e => e.ProductId)
                    .HasName ("PK__Products__B40CC6CD2734EEA5");

                entity.Property (e => e.Category)
                    .HasMaxLength (100)
                    .IsUnicode (false);

                entity.Property (e => e.Color)
                    .HasMaxLength (20)
                    .IsUnicode (false);

                entity.Property (e => e.CratedDate)
                    .HasColumnType ("datetime")
                    .HasDefaultValueSql ("(getdate())");

                entity.Property (e => e.Name)
                    .IsRequired ()
                    .HasMaxLength (100)
                    .IsUnicode (false);

                entity.Property (e => e.UnitPrice).HasColumnType ("decimal(18, 0)");
            });

            OnModelCreatingPartial (modelBuilder);
        }

        partial void OnModelCreatingPartial (ModelBuilder modelBuilder);
    }
}