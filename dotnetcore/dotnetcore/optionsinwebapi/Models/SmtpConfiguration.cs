namespace optionsinwebapi.Models {
    public class SmtpConfiguration : ISmtpConfiguration {
        public string Domain { get; set; }
        public int Port { get; set; }

        string ISmtpConfiguration.GetAllISmtpConfigurations () {
            return Domain + Port;
        }
    }
}