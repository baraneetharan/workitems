﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using optionsinwebapi.Models;

namespace optionsinwebapi.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class WeatherForecastController : ControllerBase {
        private static readonly string[] Summaries = new [] {
            "Freezing",
            "Bracing",
            "Chilly",
            "Cool",
            "Mild",
            "Warm",
            "Balmy",
            "Hot",
            "Sweltering",
            "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ISmtpConfiguration _smtpConfiguration;

        public WeatherForecastController (ILogger<WeatherForecastController> logger,ISmtpConfiguration smtpConfiguration) {
            _logger = logger;
            _smtpConfiguration = smtpConfiguration;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get () {
            var rng = new Random ();
            _logger.LogInformation(""+_smtpConfiguration.ToString());
            return Enumerable.Range (1, 5).Select (index => new WeatherForecast {
                    Date = DateTime.Now.AddDays (index),
                        TemperatureC = rng.Next (-20, 55),
                        Summary = Summaries[rng.Next (Summaries.Length)]
                })
                .ToArray ();

                
        }

        [HttpGet]
        [Route("GetSMTPDetails")]
        public int GetSMTPDetails () {
            
            return _smtpConfiguration.GetHashCode();
        }
    }
}