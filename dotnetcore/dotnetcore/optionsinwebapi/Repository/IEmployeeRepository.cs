using System.Collections.Generic;
using optionsinwebapi.Models;

namespace optionsinwebapi.Repository{
    public interface IEmployeeRepository
{
    IEnumerable<Employee> GetAllEmployees();
    Employee Add(Employee employee);
}
}