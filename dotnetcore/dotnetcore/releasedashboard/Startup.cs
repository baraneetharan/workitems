using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using releasedashboard.Models;

namespace releasedashboard {
    public class Startup {
        private const string AllowAllOriginsPolicy = "AllowAllOriginsPolicy";
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddControllers ();
            services.AddDbContext<DashboardContext> (options =>
                options.UseSqlServer (Configuration.GetConnectionString ("BloggingDatabase")));
            services.AddOpenApiDocument ();
            // Add CORS policy
            services.AddCors (options => {
                options.AddPolicy (AllowAllOriginsPolicy,
                    builder => {
                        builder.AllowAnyOrigin ()
                            .AllowAnyHeader ()
                            .AllowAnyMethod ();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseDefaultFiles();  
            app.UseStaticFiles();

            app.UseHttpsRedirection ();

            app.UseRouting ();

            app.UseCors (AllowAllOriginsPolicy);

            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
            app.UseOpenApi ();
            app.UseSwaggerUi3 ();
        }
    }
}