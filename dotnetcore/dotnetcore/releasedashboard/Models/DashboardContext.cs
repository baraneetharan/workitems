using Microsoft.EntityFrameworkCore;

namespace releasedashboard.Models
{
    public class DashboardContext : DbContext
    {
        public DashboardContext(DbContextOptions<DashboardContext> options)
            : base(options)
        {
        }

        public DbSet<Dashboard> Dashboards { get; set; }
        public DbSet<Client_Master> Client_Master { get; set; }
        public DbSet<Project_Master> Project_Master { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Project_Master>(entity =>
                {
                    entity.HasNoKey();
                    entity.ToView("Project_Master");
                });

                modelBuilder
                .Entity<Client_Master>(entity =>
                {
                    entity.HasNoKey();
                    entity.ToView("Client_Master");
                });
        }
    }
}