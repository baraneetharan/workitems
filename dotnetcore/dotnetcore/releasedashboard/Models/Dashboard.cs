using System.ComponentModel.DataAnnotations;

namespace releasedashboard.Models
{
    
public class Dashboard{

[Key]
    public decimal Release_Entry_ID { get; set; }
    public string Client_Name { get; set; }
    public string Release_Version { get; set; }
    public string Release_Type { get; set; }
    public string Environment { get; set; }
}}