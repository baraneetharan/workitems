$(document).ready(function () {
    $("select").select2();
    $("select[multiple]").select2({
        multiple: true
    });
});

var app = new function () {
    dashboards = [];
    projects = [];
    clients = [];
    
    this.changelist = function () {
        app.reset();
        var f = document.getElementById("ddlCustomers");
        var strCustomers = f.options[f.selectedIndex].innerHTML;
        fetch("https://localhost:5001/Dashboard/" + strCustomers)
            .then((response) => {
                clients = response.json();
                return clients;
            })
            .then((clients) => {
                console.log(clients);
                // ***
                var ddlCustomers = document.getElementById("ddlClients");

                //Add the Options to the DropDownList.
                for (var i = 0; i < clients.length; i++) {
                    var option = document.createElement("OPTION");

                    //Set Customer Name in Text part.
                    option.innerHTML = clients[i].client_Name;

                    //Set CustomerId in Value part.
                    option.value = clients[i].client_ID;

                    //Add the Option element to DropDownList.
                    ddlClients.options.add(option);
                }
                // ***
            });

    }

    this.getclientsandprojects = function () {
        // alert("getclientsandprojects");
        //   https://localhost:5001/Dashboard/getclients
        //   https://localhost:5001/Dashboard/getprojects);


        fetch("https://localhost:5001/Dashboard/getprojects")
            .then((response) => {
                projects = response.json();
                return projects;
            })
            .then((projects) => {
                console.log(projects);
                // ***
                var ddlCustomers = document.getElementById("ddlCustomers");

                //Add the Options to the DropDownList.
                for (var i = 0; i < projects.length; i++) {
                    var option = document.createElement("OPTION");

                    //Set Customer Name in Text part.
                    option.innerHTML = projects[i].project_Name;

                    //Set CustomerId in Value part.
                    option.value = projects[i].project_ID;

                    //Add the Option element to DropDownList.
                    ddlCustomers.options.add(option);
                }
                // ***
            });

    }

    this.getValues = function (id) {
        let result = [];
        let collection = document.querySelectorAll("#" + id + " option");
        collection.forEach(function (x) {
            if (x.selected) {
                result.push(x.innerHTML);
            }
        });
        return result;
    }

    this.FetchAll = function () {
        // alert();
        var e = document.getElementById("ddlCustomers");
        var strCustomers = e.options[e.selectedIndex].innerHTML;

        var e = document.getElementById("ddlClients");
        // var strClients = e.options[e.selectedIndex].innerHTML;
        var strClients = (app.getValues('ddlClients')).join(':');

        console.log("strCustomers " + strCustomers);
        console.log("strClients " + strClients);
        // string Client_Name,string Project_Name
        myobj = JSON.stringify({ "Client_Name": strClients, "strCustomers": strCustomers });
        // https://localhost:5001/Dashboard/26,2
        url = "https://localhost:5001/Dashboard/" + strClients + "," + strCustomers;
        fetch(url)
            .then((response) => {
                dashboards = response.json();
                console.log(dashboards);

                return dashboards;

            })
            .then((dashboards) => {
                console.log(dashboards);
                var html= "";
                for (var i = 0; i < dashboards.length; i++) {
                    html += "<tr>";
                    html += "<td>" + dashboards[i].client_Name + "</td>";
                    html += "<td>" + dashboards[i].release_Version + "</td>";
                    html += "<td>" + dashboards[i].release_Type + "</td>";
                    html += "<td>" + dashboards[i].environment + "</td>";
                    html += "<td>" + dashboards[i].release_Entry_ID + "</td>";
                    html += "</tr>";
                }
                // html += "</table>";
                document.getElementById("dashboardTableData").innerHTML = html;

                var dis = Array.from(new Set(dashboards.map((item) => item.client_Name)));
                console.log(dis);

            });
        app.reset();
    }
    // getselectionvalues
    this.getselectionvalues = function () {
        var e = document.getElementById("ddlCustomers");
        var strCustomers = e.options[e.selectedIndex].innerHTML;

        var e = document.getElementById("ddlClients");
        var strClients = e.options[e.selectedIndex].innerHTML;

        console.log("strCustomers " + strCustomers);
        console.log("strClients " + strClients);
    }

    //    getselectionvalues


    //var e = document.getElementById("ddlCustomers");
    // var strCustomers = e.options[e.selectedIndex].innerHTML;


    //  var strClients = e.options[e.selectedIndex].innerHTML;
    this.reset = function () {
        var select = document.getElementById("ddlClients");
        var length = select.options.length;
        for (i = length - 1; i >= 0; i--)
            select.options[i] = null;


    }

}