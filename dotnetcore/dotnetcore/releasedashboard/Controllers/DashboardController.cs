using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using releasedashboard.Models;

namespace releasedashboard.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class DashboardController : ControllerBase {
        private readonly DashboardContext _context;

        public DashboardController (DashboardContext context) {
            _context = context;
        }

        [HttpGet ("{Client_Name},{Project_Name}")]
        public List<Dashboard> POSTfilter (string Client_Name, string Project_Name) {
            var str1 = Client_Name.Replace ("&amp;", "&");
            var str2 = Client_Name.Replace (":", ",");
            string[] splittedArray = str2.Split (',');
            // string clts=Array.ToString(splittedArray);
            // string clts = string.Join(".", splittedArray);
            string clts = string.Join (",", splittedArray
                .Select (x => string.Format ("'{0}'", x)));
            //  str.replace('&amp;','&') 
            SqlParameter param8 = new SqlParameter ("@Client_Name", str2);
            SqlParameter param1 = new SqlParameter ("@Project_Name", Project_Name);
            //        var blogs = _context.Dashboards
            //  .FromSqlRaw("EXECUTE dbo.procedure_gitdemo8 @Client_Name={0} , @Project_Name={1}",Client_Name,Project_Name)
            //  .ToList();
            var blogs = _context.Dashboards
                .FromSqlRaw ("EXECUTE dbo.procedure_releaseDashboard @Client_Name={0} , @Project_Name={1}", param8, param1)
                .ToList ();
            return blogs;
        }

        [HttpGet]
        [Route ("getprojects")]
        public List<Project_Master> getprojects () {
            var project_Masters = _context.Project_Master
                .FromSqlRaw ("EXECUTE dbo.Project_List ")
                .ToList ();
            return project_Masters;
        }

        [HttpGet ("{Project_Name}")]
        [Route ("getclients")]
        public List<Client_Master> getclients (string Project_Name) {
            SqlParameter param1 = new SqlParameter ("@Project_Name", Project_Name);
            var Client_Lists = _context.Client_Master
                .FromSqlRaw ("EXECUTE dbo.clientlist1 @Project_Name ={0}", param1)
                .ToList ();
            return Client_Lists;
        }
    }
}