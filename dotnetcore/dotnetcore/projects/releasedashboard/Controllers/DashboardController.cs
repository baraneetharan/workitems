using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using releasedashboard.Models;

namespace releasedashboard.Controllers {
    [ApiController]
    [Route ("[controller]")]
    public class DashboardController : ControllerBase {
        private readonly DashboardContext _context;
        private readonly IAuthenticationService authService;

        public DashboardController (DashboardContext context, IAuthenticationService authService) {
            _context = context;
            this.authService = authService;
        }

        [HttpGet ("{Client_Name},{Project_Name}")]
        public List<Dashboard> POSTfilter (string Client_Name, string Project_Name) {
            var str1 = Client_Name.Replace ("&amp;", "&");
            var str2 = str1.Replace (":", ",");
            string[] splittedArray = str2.Split (',');
            // string clts=Array.ToString(splittedArray);
            // string clts = string.Join(".", splittedArray);
            string clts = string.Join (",", splittedArray
                .Select (x => string.Format ("'{0}'", x)));
            //  str.replace('&amp;','&') 
            SqlParameter param8 = new SqlParameter ("@Client_Name", str2);
            SqlParameter param1 = new SqlParameter ("@Project_Name", Project_Name);
            //        var blogs = _context.Dashboards
            //  .FromSqlRaw("EXECUTE dbo.procedure_gitdemo8 @Client_Name={0} , @Project_Name={1}",Client_Name,Project_Name)
            //  .ToList();
            var blogs = _context.Dashboards
                .FromSqlRaw ("EXECUTE dbo.procedure15 @Client_Name={0} , @Project_Name={1}", param8, param1)
                .ToList ();
            return blogs;
        }

        [HttpGet]
        [Route ("getprojects")]
        public List<Project_Master> getprojects () {
            var project_Masters = _context.Project_Master
                .FromSqlRaw ("EXECUTE dbo.Project_List ")
                .ToList ();
            return project_Masters;
        }

        [HttpGet ("{Project_Name}")]
        [Route ("getclients")]
        public List<Client_Master> getclients (string Project_Name) {
            SqlParameter param1 = new SqlParameter ("@Project_Name", Project_Name);
            var Client_Lists = _context.Client_Master
                .FromSqlRaw ("EXECUTE dbo.clientlist1 @Project_Name ={0}", param1)
                .ToList ();
            return Client_Lists;
        }

        [HttpGet]
        [Route ("Login")]
        public string Login (string userName,
            string password) {
            // List<Partner> data = await _context.Partners.Take(100).ToListAsync();
            // return Json(data);
            var user = authService.Login (userName, password);
            if (null != user) {
                string op = "passed";
                return op;
                // return new JsonResult (op);
                // return Content("{\"mystatus\": "+op);
                // return Content("{\"mystatus\": "+op+"}");
            } else {
                string op = "failed";
                return op;
                // return new JsonResult (op);
                // return Content("{\"mystatus\": "+op+"}\"");
            }

        }
        //     public string Login(string userName, string password)
        //     {
        //         var user = authService.Login(userName, password);
        //         if (null != user)
        //         {
        //              string op="passed";
        //             return op;
        //         }
        //         else 
        //         {
        //             string op="failed";
        //             return op;
        //         }
        //     }
        // }
    }
}