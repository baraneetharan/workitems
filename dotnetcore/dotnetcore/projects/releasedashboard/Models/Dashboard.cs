using System;
using System.ComponentModel.DataAnnotations;

namespace releasedashboard.Models
{
    
public class Dashboard{

[Key]
    public decimal Release_Entry_ID { get; set; }
    public string Client_Name { get; set; }
    public string Release_Version { get; set; }
    public string Release_Type { get; set; }
    public string Environment { get; set; }

    public string UAT_Deployed_Date {get;set;}
   public string PROD_Deployed_Date {get;set;}

   public DateTime Release_Date {get;set;}
}}