﻿using System;
using EASendMail;

namespace sendemail {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine ("Hello World!");
            SmtpMail oMail = new SmtpMail ("TryIt");
            SmtpClient oSmtp = new SmtpClient ();

            // Your Offic 365 email address
            oMail.From = "baraneetharan.r@kgisl.com";

            // Set recipient email address
            oMail.To = "baraneetharan@live.com";

            // Set email subject
            oMail.Subject = "test email from office 365 account";

            // Set email body
            oMail.TextBody = "this is a test email sent from c# project.";

            // Your Office 365 SMTP server address,
            // You should get it from outlook web access.
            SmtpServer oServer = new SmtpServer ("smtp.office365.com");

            // user authentication should use your
            // email address as the user name.
            oServer.User = "baraneetharan.r@kgisl.com";
            oServer.Password = "Baranee@321";

            // Set 587 port
            oServer.Port = 587;

            // detect SSL/TLS connection automatically
            oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

            try {
                Console.WriteLine ("start to send email over SSL...");
                oSmtp.SendMail (oServer, oMail);
                Console.WriteLine ("email was sent successfully!");
            } catch (Exception ep) {
                Console.WriteLine ("failed to send email with the following error:");
                Console.WriteLine (ep.Message);
            }
        }
    }
}