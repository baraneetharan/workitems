﻿using System;
using System.Collections.Generic;

namespace cscollection {
    class Program {
        static void Main (string[] args) {
            Console.WriteLine ("Hello World!");
            List<User> users = new List<User> ();

            User u1 = new User ();
            u1.Id = 1;
            u1.Name = "Name1";
            u1.Location = "CBE";

            users.Add (u1);
            
            Console.WriteLine (users);

            foreach (User item in users) {
                Console.WriteLine (item.Id);
            }
        }
    }
}