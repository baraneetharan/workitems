using System.Threading.Tasks;
using mvc3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace mvc3.Controllers {
    public class MVC3ProductsController : Controller {
        public readonly MVC3ProductsDBContext _context;
        public MVC3ProductsController (MVC3ProductsDBContext context) {
            _context = context;
        }
        public async Task<IActionResult> index () {
            return View (await _context.MVC3Products.ToListAsync ());
        }
        // GET: Products/Create
        public IActionResult Create () {
            return View ();
        }
    }
}