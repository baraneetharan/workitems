using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace mvc3.Models {
    public class MVC3Product {
        [Key]
        public long ProductId { get; set; }
        public string Name { get; set; }
    }
}