using Microsoft.EntityFrameworkCore;

namespace mvc3.Models {
    public class MVC3ProductsDBContext :DbContext {
       public MVC3ProductsDBContext(DbContextOptions<MVC3ProductsDBContext> options)
            : base(options)
        {
        }
        public DbSet<MVC3Product> MVC3Products { get; set; }
    }
}