using Microsoft.EntityFrameworkCore;


namespace flipkartdemo.Models {
    public class MyCartContext : DbContext {

        public MyCartContext (DbContextOptions<MyCartContext> options) : base (options) { }
        public DbSet<MyCart> MyCarts { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }

       
    }
}