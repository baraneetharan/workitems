using System;

namespace flipkartdemo.Models
{
    public class MyCart
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public float Amount { get; set; }

                public static MyCart FromCsv (string csvLine) {
            string[] values = csvLine.Split (',');
            MyCart myCartValues = new MyCart ();
            myCartValues.Id = 0;
            myCartValues.Name = Convert.ToString (values[1]);
            myCartValues.Quantity = int.Parse(values[2]);//Convert.ToInt32 (values[2]);
            myCartValues.Price = float. Parse(values[3]);
            myCartValues.Amount = myCartValues.Quantity* myCartValues.Price;
            return myCartValues;
        }

    }
}