using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using flipkartdemo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace flipkartdemo.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class MyCartsController : ControllerBase {
        private readonly SmtpConfiguration _smtpConfiguration;

        private readonly MyCartContext _context;

        public MyCartsController (MyCartContext context,SmtpConfiguration smtpConfiguration) {
            _context = context;
            _smtpConfiguration=smtpConfiguration;
        }

        // GET: api/MyCarts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MyCart>>> GetMyCarts () {
            return await _context.MyCarts.ToListAsync ();
        }

        // GET: api/MyCarts/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<MyCart>> GetMyCart (long id) {
            var myCart = await _context.MyCarts.FindAsync (id);

            if (myCart == null) {
                return NotFound ();
            }

            return myCart;
        }

        // PUT: api/MyCarts/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutMyCart (long id, MyCart myCart) {
            if (id != myCart.Id) {
                return BadRequest ();
            }

            _context.Entry (myCart).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!MyCartExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/MyCarts
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MyCart>> PostMyCart (MyCart myCart) {
            _context.MyCarts.Add (myCart);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetMyCart", new { id = myCart.Id }, myCart);
        }

        [HttpPost]
        [Route ("readfromcsv")]
        public async Task<List<MyCart>> readmycartsAsync () {
            List<MyCart> myCartValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => MyCart.FromCsv (v)).ToList ();;
            foreach (MyCart x in myCartValues) {
                _context.MyCarts.Add (x);
                await _context.SaveChangesAsync ();
            }
            return myCartValues;

        }

        // DELETE: api/MyCarts/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<MyCart>> DeleteMyCart (long id) {
            var myCart = await _context.MyCarts.FindAsync (id);
            if (myCart == null) {
                return NotFound ();
            }

            _context.MyCarts.Remove (myCart);
            await _context.SaveChangesAsync ();

            return myCart;
        }

        private bool MyCartExists (long id) {
            return _context.MyCarts.Any (e => e.Id == id);
        }
    }
}