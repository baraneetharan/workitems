using Microsoft.AspNetCore.Mvc;
using optdemo.Models;

namespace optionsinwebapi.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class DemoConfigController : ControllerBase {
        private readonly ISmtpConfiguration _smtpConfiguration;

        public DemoConfigController (ISmtpConfiguration smtpConfiguration) {
            _smtpConfiguration = smtpConfiguration;
        }

        [HttpGet]
        [Route ("DemoConfigGetSMTPDetails")]
        public int GetSMTPDetails () {
            return _smtpConfiguration.GetHashCode ();
        }
           [HttpGet]
        [Route ("GetSMTPDetails")]
        public int GetSMTPDetails1 () {
            return _smtpConfiguration.GetHashCode ();
        }

    }
}