using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ShoppingCart.Models;

namespace ShoppingCart.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase {
        private readonly ShoppingCartDbContext _context;

        public ProductsController (ShoppingCartDbContext context) {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts () {
            return await _context.Products.ToListAsync ();
        }

        [HttpGet]
        [Route ("calculation")]
        // [Produces ("application/json")]
        public IEnumerable<string> Get () {
            List<Product> x = _context.Products.ToList();
            return new string[] { JsonConvert.SerializeObject(x), "value2" };
        }

        // GET: api/Products/5
        [HttpGet ("{id}")]
        public async Task<ActionResult<Product>> GetProduct (int id) {
            var product = await _context.Products.FindAsync (id);

            if (product == null) {
                return NotFound ();
            }

            return product;
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut ("{id}")]
        public async Task<IActionResult> PutProduct (int id, Product product) {
            if (id != product.TransID) {
                return BadRequest ();
            }

            _context.Entry (product).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync ();
            } catch (DbUpdateConcurrencyException) {
                if (!ProductExists (id)) {
                    return NotFound ();
                } else {
                    throw;
                }
            }

            return NoContent ();
        }

        // POST: api/Products
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct (Product product) {
            _context.Products.Add (product);
            await _context.SaveChangesAsync ();

            return CreatedAtAction ("GetProduct", new { id = product.TransID }, product);
        }

        [HttpPost]
        [Route ("readfromcsv")]
        public async Task<List<Product>> readmycartsAsync () {
            List<Product> myCartValues = System.IO.File.ReadAllLines (Environment.CurrentDirectory + "\\data.csv").Skip (1).Select (v => Product.FromCsv (v)).ToList ();;

            _context.Products.AddRange (myCartValues);
            await _context.SaveChangesAsync ();

            return myCartValues;

        }

        // DELETE: api/Products/5
        [HttpDelete ("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct (int id) {
            var product = await _context.Products.FindAsync (id);
            if (product == null) {
                return NotFound ();
            }

            _context.Products.Remove (product);
            await _context.SaveChangesAsync ();

            return product;
        }

        private bool ProductExists (int id) {
            return _context.Products.Any (e => e.TransID == id);
        }
    }
}