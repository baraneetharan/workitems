using System;
using System.ComponentModel.DataAnnotations;

namespace ShoppingCart
{
    
}
public class Product
{
    [Key]
    public int TransID { get; set; }
    public string TransDate { get; set; }
    public int CustID { get; set; }
    public string Category { get; set; }
    public string ProductName { get; set; }
    public int Qty { get; set; }
    public float Price { get; set; }

    public static Product FromCsv (string csvLine) {
            string[] values = csvLine.Split (',');
            Product myCartValues = new Product ();
            myCartValues.TransID = 0;
            myCartValues.TransDate = Convert.ToString (values[1]);
            myCartValues.CustID = int.Parse(values[2]);
            myCartValues.Category = Convert.ToString(values[3]);
            myCartValues.ProductName = Convert.ToString(values[4]);
            myCartValues.Qty = int.Parse(values[5]);
            myCartValues.Price = float.Parse(values[6]);
            return myCartValues;
        }
}