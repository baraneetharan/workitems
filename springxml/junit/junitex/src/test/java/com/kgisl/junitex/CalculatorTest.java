package com.kgisl.junitex;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;


/**
 * JunitExample
 */
public class CalculatorTest {
    public static Calculator c;
    @BeforeClass
    public static void setup() {
        System.out.println("BeforeClass");
        c =new Calculator();
    }
@Before
    public void before() {
        System.out.println("before");
    }
@After
    public void after() {
        System.out.println("after");
    }
@AfterClass
    public static void afterclass() {
        System.out.println("afterclass");
    }
@Test
    public void addTest() {
        // AAA
        // Assignments
        // Action
        int actual=c.add(10, 20);
        int expected=30;
        // Assert
        System.out.println("testmethod1");
        assertEquals(expected, actual);
    }
@Test
    public void subTest() {
        int actual=c.sub(10, 20);
        int expected=-10;
        // Assert
        assertEquals(expected, actual);
    }
@Ignore
    public void mulTest() {
        int actual=c.mul(10, 20);
        int expected=200;
        // Assert
        assertEquals(expected, actual);
    }
}