package com.kgisl.mvnjunit;

public class ColorPencil extends Pencil {
    private String color;

    ColorPencil(String color) {
        this.setColor(color);
    }

    public ColorPencil() {
	}

	public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
