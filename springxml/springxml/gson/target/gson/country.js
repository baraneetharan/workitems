var app = new function () {
    var result;
    var countries = [];
    // var countries = [{ "name": "India", "id": 1 },
    // { "name": "France", "id": 2 },
    // { "name": "Germany", "id": 3 },
    // { "name": "England", "id": 4 },
    // { "name": "Spain", "id": 5 },
    // { "name": "USA", "id": 6 }];

    var mode = "";
    var updateidx;

    this.FetchAll = function () {
        countries = [];
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                result = xhr.responseText;
                console.log(result);
                countries = JSON.parse(result);
            }
        }
        xhr.open('GET', '/country', true);
        xhr.send(null);
        // console.log(countries);
        this.FetchAll();
    }

    this.printtable = function () {
        console.log(countries);
        this.el = document.getElementById("countries");
        var table_body = '<table border="1" id="example"><thead><tr><th>id</th><th>name</th></tr></thead><tbody>';
        for (var i in countries) {
            table_body += '<tr>';
            table_body += '<td>';
            table_body += countries[i].id;
            table_body += '</td>';
            table_body += '<td>';
            table_body += countries[i].name;
            table_body += '</td>';
            table_body += '<td><button onclick="app.editMethod(' + countries[i].id + ')">Edit</button></td>';
            table_body += '<td><button onclick="app.deleteMethod(' + countries[i].id + ')">Delete</button></td>';
            table_body += '</tr>';
        }
        this.el.innerHTML = table_body;

        // this.el = document.getElementById('countries');
        // //console.log("getAllCountries");
        // var data = '';
        // if (countries.length > 0) {

        //     for (i = 0; i < countries.length; i++) {

        //         data += '<tr>';
        //         data += '<tr>';
        //         data += '<td>' + countries[i].id + '</td>';
        //         data += '<td>' + countries[i].name + '</td>';
        //         data += '<td><button onclick="app.editCountry(' + countries[i].id + ')">Edit</button></td>';
        //         data += '<td><button onclick="app.deleteCountry(' + countries[i].id + ')">Delete</button></td>';
        //         data += '</tr>';

        //     }
        // }

        // this.el.innerHTML = data;
    };


    this.insertCountry = function () {
        //console.log("insertCountry");
    }

    this.getCountry = function (id) {
        //console.log("getCountry");
    }

    this.editMethod = function (id) {
        //console.log("editCountry " + id);
        mode = "edit";
        document.getElementById('btn').innerHTML = "Update";
        for (let index = 0; index < countries.length; index++) {
            // const element = array[index];
            //console.log(countries[index].id);
            if (countries[index].id == id) {
                //console.log(countries[index].id);
                updateidx = index;
                document.getElementById("id").value = countries[index].id;
                document.getElementById("name").value = countries[index].name;
                document.getElementById('id').readOnly = true;

            }

        }
    }

    this.deleteMethod = function (id) {

        console.log("delete called " + id);
        var url = "/country";
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", url + '/?id=' + id, true);
        xhr.onload = function () {
            // var users = JSON.parse(xhr.responseText);
            if (xhr.readyState == 4 && xhr.status == "200") {
                // console.table(users);
            } else {
                // console.error(users);
            }
        }
        xhr.send(null);
        //  var id = document.getElementById("id").value;
        // var name = document.getElementById('name').value;
        // countries.pop();
        //console.log("tobedeleted " + id);

        // for (let index = 0; index < countries.length; index++) {
        //     // const element = array[index];
        //     //console.log(countries[index].id);
        //     if (countries[index].id == id) {
        //         //console.log(countries[index]);
        //         countries.splice(index, 1);
        //     }

        // }

        // countries.splice(countries.indexOf(id), 1);
        //console.log(countries);
        // this.getAllCountries();

    }

    this.saveorupdate = function (id) {
        //console.log("saveorupdate");
        var id = document.getElementById("id").value;
        var name = document.getElementById('name').value;
        //console.log(id + "-->" + name);
        if (mode == "") {

            var url = "/country";

            // data.id = id;
            // data.name = name;
            data = { "id": id, "name": name };
            var json = JSON.stringify(data);

            var xhr = new XMLHttpRequest();
            // xhr.open("POST", url+'/?id=' + id, true);
            xhr.open("PUT", url + '/?id=' + id + '&name=' + name, true);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            xhr.onload = function () {
                alert(xhr.responseText);
                // var users = JSON.parse(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "201") {
                    this.FetchAll();  // console.table(users);
                } else {
                    // console.error(users);
                }
            }
            xhr.send(json);
            this.FetchAll();
            // var newcountry = { "id": id, "name": name };
            // //console.log(newcountry);
            // // .push({"01": nieto.label, "02": nieto.value});
            // countries.push(newcountry);
            // //console.log(JSON.stringify(countries));

        }
        else {
            console.log("update called " + id);
            var updatecountry = { "id": id, "name": name };

            var url = "/country";

            data = { "id": id, "name": name };
            var json = JSON.stringify(data);

            var xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            xhr.onload = function () {
                alert(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "201") {
                    // console.table(users);
                } else {
                    // console.error(users);
                }
            }
            xhr.send(json);
            //console.log(updatecountry);
            // .push({"01": nieto.label, "02": nieto.value});
            // countries.push(updatecountry);
            //console.log("updateidx " + updateidx);
            // countries.splice(updateidx, 1, updatecountry);
            document.getElementById('btn').innerHTML = "Add";
            // this.FetchAll();
        }
        document.getElementById("id").value = "";
        document.getElementById("name").value = "";
        // this.FetchAll();
    }

};