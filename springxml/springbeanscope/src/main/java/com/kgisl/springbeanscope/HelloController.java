package com.kgisl.springbeanscope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HelloController
 */
@RestController
@RequestMapping("/")
public class HelloController {

    @Autowired
    private GreetingService greetingService;
    @GetMapping
    public String getall() {
        String myGreet=greetingService.greet();
        System.out.println(greetingService);
        return "HelloController "+myGreet;
    }

}