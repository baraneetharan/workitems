package com.kgisl.springbeanscope;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * GreetingService
 */
@Component
// @Scope("singleton")
// @Scope("prototype")
@Scope(value="request", proxyMode =ScopedProxyMode.TARGET_CLASS)
public class GreetingService {

    public String greet(){
        return "Hello";
    }
}