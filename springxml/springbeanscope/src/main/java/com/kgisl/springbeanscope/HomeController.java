package com.kgisl.springbeanscope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HelloController
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private GreetingService greetingService;
    @GetMapping
    public String getall() {
        String myGreet=greetingService.greet();
        System.out.println(greetingService);
        return "HomeController "+myGreet;
    }
    @PutMapping
    public String put() {
        String myGreet=greetingService.greet();
        System.out.println(greetingService);
        return "HomeController "+myGreet;
    }
    @DeleteMapping
    public String delete() {
        String myGreet=greetingService.greet();
        System.out.println(greetingService);
        return "HomeController "+myGreet;
    }
    
}