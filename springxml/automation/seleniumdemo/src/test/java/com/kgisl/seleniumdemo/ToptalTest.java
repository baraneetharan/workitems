package com.kgisl.seleniumdemo;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ToptalTest {
    WebDriver driver;

    @Before
    public void setup() throws InterruptedException {
        // use FF Driver
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\baraneetharan.r\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://www.google.com/");
        Thread.sleep(5000); // Let the user actually see something!
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void toptalLoginTest() throws InterruptedException {
        System.out.println("toptalLoginTest runs ...");
        

    }

    @Test
    public void applyAsFreelancerTest() throws InterruptedException {
        System.out.println("applyAsFreelancerTest runs");

        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("ChromeDriver");
        searchBox.submit();

        Thread.sleep(5000); // Let the user actually see something!
        // driver.quit();
    }

    @After
    public void close() {
        driver.close();
    }

}