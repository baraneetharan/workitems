package com.kgisl.demospringboot.repository;

import com.kgisl.demospringboot.Models.Team;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * TeamRepository
 */
public interface TeamRepository extends JpaRepository<Team,Long>{

    
}