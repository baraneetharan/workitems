package com.kgisl.aopdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HomeController
 */
@RestController
@RequestMapping("/home")
public class HomeController {
    Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    public String greeting(){
        LOGGER.error("error");
        LOGGER.warn("warn");
        LOGGER.info("info");
        LOGGER.de("debug");
        
        return "Hello";
    }

}