namespace readtalentxjsonfile
{
    class Punch
    {
        // slno date day in out hour leave               
        public string pdate { get; set; }
        public string day { get; set; }
        public string intime { get; set; }
        public string outtime { get; set; }
        public string tothour { get; set; }
        public string holidaydesc { get; set; }

        public Punch(string pdate, string day, string intime, string outtime,string tothour, string holidaydesc)
        {
            this.pdate = pdate;
            this.day = day;
            this.intime = intime;
            this.outtime = outtime;
            this.tothour=tothour;
            this.holidaydesc=holidaydesc;
        }

    }
}