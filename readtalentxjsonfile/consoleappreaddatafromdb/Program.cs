﻿using System;
using System.IO;
using ConsoleApp.DAL;
using Microsoft.Extensions.Configuration;

namespace consoleappreaddatafromdb
{
    class Program
    {
        private static IConfiguration _iconfiguration;  
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            GetAppSettingsFile();
            PrintCountries();
        }
        static void GetAppSettingsFile()
        {
            // var builder = new ConfigurationBuilder()
            //                      .SetBasePath(Directory.GetCurrentDirectory())
            //                      .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            // _iconfiguration = builder.Build();
            _iconfiguration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .Build();
        }
        static void PrintCountries()
        {
            var countryDAL = new CountryDAL(_iconfiguration);
            var listCountryModel = countryDAL.GetList();
            listCountryModel.ForEach(item =>
            {
                Console.WriteLine(item.Country);
            });
            Console.WriteLine("Press any key to stop.");
            Console.ReadKey();
        }
    }
}
