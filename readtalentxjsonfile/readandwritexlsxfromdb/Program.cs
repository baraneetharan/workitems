﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace readandwritexlsxfromdb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            IWorkbook workbook = new XSSFWorkbook();

            // ... write some data to the workbook ...

            using (FileStream stream = new FileStream("outfile.xlsx", FileMode.Create, FileAccess.Write))
            {
                workbook.Write(stream);
            }

            WriteExcel();
        }

        static void WriteExcel()
        {
            List<UserDetails> persons = new List<UserDetails>()
            {
                new UserDetails() {ID="1001", Name="ABCD", City ="City1", Country="USA"},
                new UserDetails() {ID="1002", Name="PQRS", City ="City2", Country="INDIA"},
                new UserDetails() {ID="1003", Name="XYZZ", City ="City3", Country="CHINA"},
                new UserDetails() {ID="1004", Name="LMNO", City ="City4", Country="UK"},
           };

            // Lets converts our object data to Datatable for a simplified logic.
            // Datatable is most easy way to deal with complex datatypes for easy reading and formatting.

            DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(persons), (typeof(DataTable)));
            var memoryStream = new MemoryStream();

            using (var fs = new FileStream("Result.xlsx", FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Sheet1");
                
                excelSheet.SetColumnWidth(1, 12* 256);
                excelSheet.SetColumnWidth(3, 25 * 256);

                //font style1: underlined, italic, red color, fontsize=20
                IFont font1 = workbook.CreateFont();
                font1.Color = IndexedColors.Red.Index;
                font1.IsItalic = true;
                font1.Underline = FontUnderlineType.Double;
                font1.FontHeightInPoints = 20;

                //bind font with style 1
                // ICellStyle style1 = workbook.CreateCellStyle();
                // style1.SetFont(font1);

                ICellStyle cellStyleBlue = workbook.CreateCellStyle();
                cellStyleBlue.FillForegroundColor = IndexedColors.LightBlue.Index;
                cellStyleBlue.FillPattern = FillPattern.SolidForeground;
                cellStyleBlue.SetFont(font1);



                List<String> columns = new List<string>();
                IRow row = excelSheet.CreateRow(0);
                int columnIndex = 0;

                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);
                    row.CreateCell(columnIndex).SetCellValue(column.ColumnName);
                    row.Cells[columnIndex].CellStyle = cellStyleBlue;
                    columnIndex++;
                }

                int rowIndex = 1;
                foreach (DataRow dsrow in table.Rows)
                {
                    row = excelSheet.CreateRow(rowIndex);
                    int cellIndex = 0;
                    foreach (String col in columns)
                    {
                        row.CreateCell(cellIndex).SetCellValue(dsrow[col].ToString());
                        cellIndex++;
                    }

                    rowIndex++;
                }
                workbook.Write(fs);
            }

        }
    }
}
