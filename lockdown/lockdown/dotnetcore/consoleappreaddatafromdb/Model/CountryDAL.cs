using consoleappreaddatafromdb.Model;  
using Microsoft.Extensions.Configuration;  
using System;  
using System.Collections.Generic;  
using System.Data;  
using System.Data.SqlClient;  
namespace ConsoleApp.DAL  
{  
public class CountryDAL  
{  
   private string _connectionString;  
   public CountryDAL(IConfiguration iconfiguration)  
   {  
      // _connectionString = iconfiguration.GetConnectionString("Default");  
      _connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\baraneetharan.r\\Documents\\localmdffile.mdf;Integrated Security=True;Connect Timeout=30";  
   }  
   public List<CountryModel> GetList()  
   {  
      var listCountryModel = new List<CountryModel>();  
      try  
      {  
         using (SqlConnection con = new SqlConnection(_connectionString))  
         {  
               SqlCommand cmd = new SqlCommand("SP_COUNTRY_GET_LIST", con);  
               cmd.CommandType = CommandType.StoredProcedure;  
               con.Open();  
               SqlDataReader rdr = cmd.ExecuteReader();  
               while (rdr.Read())  
            {  
                  listCountryModel.Add(new CountryModel  
                   {  
                           Id = Convert.ToInt32(rdr[0]),  
                           Country = rdr[1].ToString(),  
                           Active = Convert.ToBoolean(rdr[2])  
                     });  
               }                 
            }  
         }  
         catch (Exception ex)  
         {  
               throw ex;  
         }  
         return listCountryModel;  
      }  
   }  
}