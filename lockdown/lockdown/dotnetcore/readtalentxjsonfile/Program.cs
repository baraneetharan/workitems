﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace readtalentxjsonfile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            readjson();
        }
        public static void readjson()
        {
            using (StreamReader r = new StreamReader("D:\\baranee\\lockdown\\dotnetcore\\readtalentxjsonfile\\allemployeespunchdata.json"))
            {
                string jsonstr = r.ReadToEnd();
                dynamic data = JObject.Parse(jsonstr);
                // Console.WriteLine(data1);

                foreach (var emp in data.response)
                {
                    var empcode = emp.Path.Substring(emp.Path.IndexOf(".") + 1);
                    // Write file Name here
                    // Console.WriteLine(empcode);

                    Console.WriteLine("#############################");
                    // Console.WriteLine("Print " + empcode + " Details");

                    dynamic cal = data.response[empcode]["Calendar"];
                    dynamic associate_code = data.response[empcode]["AssociateDetail"]["associate_code"];
                    dynamic associate_name = data.response[empcode]["AssociateDetail"]["associate_name"];
                    dynamic email = data.response[empcode]["AssociateDetail"]["email"];
                    dynamic title = data.response[empcode]["AssociateDetail"]["title"];
                    dynamic department = data.response[empcode]["AssociateDetail"]["department"];

                    // Console.WriteLine(associate_code);
                    // Console.WriteLine(associate_name);
                    // Console.WriteLine(email);
                    // Console.WriteLine(title);
                    // Console.WriteLine(department);

                    IList<Punch> punchList = new List<Punch>();
                    foreach (var c in cal)
                    {
                        var pdate = c.Path.Substring(c.Path.Length - 10);
                        // Console.WriteLine(pdate);
                        // holiday_desc
                        dynamic holiday_desc = data.response[empcode]["Calendar"][pdate]["holiday_desc"];
                        // Console.WriteLine(holiday_desc);
                        // is_holiday
                        dynamic is_holiday = data.response[empcode]["Calendar"][pdate]["is_holiday"];
                        // Console.WriteLine(is_holiday);
                        // is_employee_leave
                        dynamic is_employee_leave = data.response[empcode]["Calendar"][pdate]["is_employee_leave"];
                        var is_el = is_employee_leave != null ? is_employee_leave : "NA";
                        // Console.WriteLine(is_el);

                        // leave_name
                        dynamic leave_name = data.response[empcode]["Calendar"][pdate]["leave_name"];
                        var ln = leave_name != null ? leave_name : "";
                        // Console.WriteLine(ln);
                        // emp_leave_session
                        dynamic emp_leave_session = data.response[empcode]["Calendar"][pdate]["emp_leave_session"];
                        var els = emp_leave_session != null ? emp_leave_session : "NA";
                        // Console.WriteLine(els);
                        // punch_time
                        JArray punch_time = (JArray)data.response[empcode]["Calendar"][pdate]["punch_time"];
                        string intime = "";
                        string outtime = "";
                        if (punch_time != null && punch_time.Count == 2)
                        {
                            intime = (string)punch_time[0].ToString();
                            outtime = (string)punch_time[1].ToString();
                            // Console.WriteLine("intime---->" + intime);
                            // Console.WriteLine("outtime---->" + outtime);
                        }
                        if (punch_time != null && punch_time.Count == 1)
                        {
                            intime = (string)punch_time[0].ToString();
                            // Console.WriteLine("intime---->" + intime);
                        }
                        // Write array
                        //  slno date day in out hour leave               
                        // punchList = new List<Punch>();
                        string pday = (DateTime.Parse(pdate).DayOfWeek).ToString();
                        string hur = "";
                        if (outtime != "")
                        {
                            hur = (DateTime.Parse(outtime) - DateTime.Parse(intime)).ToString();
                        }
                        else
                        {
                            // Console.WriteLine(array.intime);
                            // hur = (DateTime.Parse(array.intime.Substring(0,10)+" 12:00:00") - DateTime.Parse(array.intime)).ToString();
                            hur = "";
                        }
                        string holiday = (string)holiday_desc != "" ? (string)holiday_desc : (string)ln;
                        if (holiday == "" && intime == "")
                        {
                            // if(intime == ""){
                            holiday = "MisPunch";
                            // }
                        }
                        punchList.Add(new Punch((string)pdate, pday, intime, outtime, hur, holiday));
                    }
                    // print employee details
                    Console.WriteLine(associate_code);
                    Console.WriteLine(associate_name);
                    Console.WriteLine(email);
                    Console.WriteLine(title);
                    Console.WriteLine(department);
                    // print punchlist

                    foreach (var array in punchList)
                    {
                        string hur = "";
                        if (array.outtime != "")
                        {
                            hur = (DateTime.Parse(array.outtime) - DateTime.Parse(array.intime)).ToString();
                        }
                        else
                        {
                            // Console.WriteLine(array.intime);
                            // hur = (DateTime.Parse(array.intime.Substring(0,10)+" 12:00:00") - DateTime.Parse(array.intime)).ToString();
                            hur = "";
                        }
                        // string hur=x.ToString();
                        Console.WriteLine(array.pdate + " | " + DateTime.Parse(array.pdate).DayOfWeek + " | " + array.intime + " | " + array.outtime + " | " + hur + " | " + array.holidaydesc);

                    }

                    // write xlsx

                    var memoryStream = new MemoryStream();
                    // D:\baranee\lockdown\dotnetcore\xlsxfilelocation
                    using (var fs = new FileStream("D:\\baranee\\lockdown\\dotnetcore\\xlsxfilelocation\\" + "T" + associate_code + ".xlsx", FileMode.Create, FileAccess.Write))
                    {
                        IWorkbook workbook = new XSSFWorkbook();
                        ISheet excelSheet = workbook.CreateSheet("Sheet1");

                        IRow row1 = excelSheet.CreateRow(1);
                        row1.CreateCell(0).SetCellValue("KG INFORMATION SYSTEMS SDN BHD.(1043643-K)");

                        IRow row2 = excelSheet.CreateRow(2);
                        row2.CreateCell(0).SetCellValue("Timesheet");

                        IRow row5 = excelSheet.CreateRow(5);
                        row5.CreateCell(0).SetCellValue("EMPLOYEE NAME : ");
                        row5.CreateCell(1).SetCellValue((string)associate_name);

                        IRow row6 = excelSheet.CreateRow(6);
                        row6.CreateCell(0).SetCellValue("EMAIL");
                        row6.CreateCell(1).SetCellValue((string)email);

                        IRow row7 = excelSheet.CreateRow(7);
                        row7.CreateCell(0).SetCellValue("MONTH : ");
                        // row7.CreateCell(1).SetCellValue(emp.Month);

                        IRow row8 = excelSheet.CreateRow(8);
                        row8.CreateCell(0).SetCellValue("RESOURCE TITLE : ");
                        // row8.CreateCell(1).SetCellValue(emp.ResourceTitle);

                        IRow row9 = excelSheet.CreateRow(9);
                        row9.CreateCell(0).SetCellValue("PROJECT NAME : ");
                        // row9.CreateCell(1).SetCellValue(emp.ProjectName);

                        IRow row3 = excelSheet.CreateRow(10);
                        IRow row4 = excelSheet.CreateRow(11);

                        // IRow rowpunch = excelSheet.CreateRow(5);
                        // rowpunch.CreateCell(0).SetCellValue("slno");
                        // rowpunch.CreateCell(1).SetCellValue("pdate");
                        // rowpunch.CreateCell(2).SetCellValue("pday");

                        DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(punchList), (typeof(DataTable)));
                        List<String> columns = new List<string>();
                        IRow punchrow = excelSheet.CreateRow(12);
                        int columnIndex = 0;

                        foreach (System.Data.DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);
                            punchrow.CreateCell(columnIndex).SetCellValue(column.ColumnName);
                            columnIndex++;
                        }

                        int rowIndex = 13;
                        foreach (DataRow dsrow in table.Rows)
                        {
                            punchrow = excelSheet.CreateRow(rowIndex);
                            int cellIndex = 0;
                            foreach (String col in columns)
                            {
                                punchrow.CreateCell(cellIndex).SetCellValue(dsrow[col].ToString());
                                cellIndex++;
                            }

                            rowIndex++;
                        }
                        workbook.Write(fs);
                    }

                }
            }
        }
    }
}