```
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace readtalentxjsonfile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            readjson();
        }
        public static void readjson()
        {
            using (StreamReader r = new StreamReader("D:\\baranee\\lockdown\\dotnetcore\\readtalentxjsonfile\\allemployeespunchdata.json"))
            {
                string jsonstr = r.ReadToEnd();
                dynamic data = JObject.Parse(jsonstr);
                // Console.WriteLine(data1);

                foreach (var emp in data.response)
                {
                    var empcode = emp.Path.Substring(emp.Path.IndexOf(".") + 1);
                    // Write file Name here
                    // Console.WriteLine(empcode);

                    Console.WriteLine("#############################");
                    Console.WriteLine("Print " + empcode + " Details");

                    dynamic cal = data.response[empcode]["Calendar"];
                    dynamic associate_code = data.response[empcode]["AssociateDetail"]["associate_code"];
                    dynamic associate_name = data.response[empcode]["AssociateDetail"]["associate_name"];
                    dynamic email = data.response[empcode]["AssociateDetail"]["email"];
                    dynamic title = data.response[empcode]["AssociateDetail"]["title"];
                    dynamic department = data.response[empcode]["AssociateDetail"]["department"];

                    Console.WriteLine(associate_code);
                    Console.WriteLine(associate_name);
                    Console.WriteLine(email);
                    Console.WriteLine(title);
                    Console.WriteLine(department);

                    IList<Punch> punchList = new List<Punch>();
                    foreach (var c in cal)
                    {
                        var pdate = c.Path.Substring(c.Path.Length - 10);
                        // Console.WriteLine(pdate);
                        // holiday_desc
                        dynamic holiday_desc = data.response[empcode]["Calendar"][pdate]["holiday_desc"];
                        // Console.WriteLine(holiday_desc);
                        // is_holiday
                        dynamic is_holiday = data.response[empcode]["Calendar"][pdate]["is_holiday"];
                        // Console.WriteLine(is_holiday);
                        // is_employee_leave
                        dynamic is_employee_leave = data.response[empcode]["Calendar"][pdate]["is_employee_leave"];
                        var is_el = is_employee_leave != null ? is_employee_leave : "NA";
                        // Console.WriteLine(is_el);

                        // leave_name
                        dynamic leave_name = data.response[empcode]["Calendar"][pdate]["leave_name"];
                        var ln = leave_name != null ? leave_name : "NA";
                        // Console.WriteLine(ln);
                        // emp_leave_session
                        dynamic emp_leave_session = data.response[empcode]["Calendar"][pdate]["emp_leave_session"];
                        var els = emp_leave_session != null ? emp_leave_session : "NA";
                        // Console.WriteLine(els);
                        // punch_time
                        JArray punch_time = (JArray)data.response[empcode]["Calendar"][pdate]["punch_time"];
                        string intime = "";
                        string outtime = "";
                        if (punch_time != null && punch_time.Count == 2)
                        {
                            intime = (string)punch_time[0].ToString();
                            outtime = (string)punch_time[1].ToString();
                            // Console.WriteLine("intime---->" + intime);
                            // Console.WriteLine("outtime---->" + outtime);
                        }
                        if (punch_time != null && punch_time.Count == 1)
                        {
                            intime = (string)punch_time[0].ToString();
                            // Console.WriteLine("intime---->" + intime);
                        }
                        // Write array
                        //  slno date day in out hour leave               
                        // punchList = new List<Punch>();
                        punchList.Add(new Punch((string)pdate, "day", intime, outtime, "hour", (string)holiday_desc));
                    }
                    // print punchlist

                    foreach (var array in punchList)
                    {
                            string hur ="";
                        if (array.outtime != "")
                        {
                            hur = (DateTime.Parse(array.outtime) - DateTime.Parse(array.intime)).ToString();
                        }
                        else{
                            // Console.WriteLine(array.intime);
                            // hur = (DateTime.Parse(array.intime.Substring(0,10)+" 12:00:00") - DateTime.Parse(array.intime)).ToString();
                            hur="";
                        }
                        // string hur=x.ToString();
                        Console.WriteLine(array.pdate + " | " + DateTime.Parse(array.pdate).DayOfWeek + " | " + array.intime + " | " + array.outtime + " | " + hur + " | " + array.holidaydesc);
                    }
                }
            }
        }
    }
}
```