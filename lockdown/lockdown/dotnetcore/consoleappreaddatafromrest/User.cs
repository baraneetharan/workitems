using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace consoleappreaddatafromrest
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string email { get; set; }

        public async Task RunAsync()
        {
            string url = "https://dotnetcorewebapi.herokuapp.com/api/Users";

            HttpClient client = new HttpClient();

            string response = await client.GetStringAsync(url);
            
            List<User> data = JsonConvert.DeserializeObject<List<User>>(response);
            foreach (User room in data)
            {
                long id = room.Id;
                string name = room.Name;
                string email = room.email;
                Console.WriteLine(id + " " + name+ " " + email);            
            }
        }
    }
}