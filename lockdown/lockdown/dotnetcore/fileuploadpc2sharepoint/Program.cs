﻿using System;
using System.Net;
using System.Security;
using Microsoft.SharePoint.Client;

namespace fileuploadpc2sharepoint
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");


            // https://kgislgroup.sharepoint.com/sites/ZeroCodeDevelopment

            using (var clientContext = new ClientContext("https://kgislgroup.sharepoint.com/sites/ZeroCodeDevelopment"))
            {
                // SecureString SecurePassword = new NetworkCredential("", "Baraneekg@12").SecurePassword;
                SecureString SecurePassword = GetSecureString("password");
                // clientContext.Credentials = new SharePointOnlineCredentials("Domain\\Username", SecurePassword);
                clientContext.Credentials = new SharePointOnlineCredentials("baraneetharan.r@kgisl.com", SecurePassword);

                Web web = clientContext.Web;
                clientContext.Load(web,
                webSite => webSite.Title);
                clientContext.ExecuteQuery();
                Console.WriteLine(web.Title);
            }
        }
    }
}
